/**
 * Mapa Socioambiental HTTP Server.
 * Thanks https://github.com/sfarthin/express-ssi-example
 */

'use strict';

// Requirements
var express = require("express"),
  ssi       = require("ssi"),
  path      = require("path"),
  fs        = require("fs"),
  app       = express();

// Parameters
var version = '/v2';
var matcher = "/**/*.html";
var parser  = new ssi(__dirname, "", "", matcher);

app.use('/images', express.static(path.join(__dirname, 'images')))

// Dispatch
app.use(function(req, res, next) {
  // Main redirect
  if (req.url == '/') {
    res.redirect(version + '/index.html');
  }

  var filename = __dirname + (req.path == "/" ? "/index.html" : req.path);

  if (fs.existsSync(filename)) {
    res.send(parser.parse(filename, fs.readFileSync(filename, {encoding: "utf8"})).contents);
  } else {
    next();
  }
});

// Run
app.listen(process.env.PORT || 80);
