# Mapa.Eco.BR - A Portable Socioenvironmental Map!

An embeddable mapping library to enhance
[the Power of Maps](https://www.youtube.com/watch?time_continue=2&v=TIVDPP9IJPo).

It's highly customizable and comes by default with many relevant layers for
the brazilian territory such as:

* Indigenous Lands and Conservation Areas.
* Fire spots in the last 24 hours.
* Accumulated deforestation.
* Biomes and watersheds.

## See it in action

* [Live Instance](https://mapa.eco.br).
* [Examples](https://mapa.socioambiental.org/pages/examples/?lang=en).
* [About](https://mapa.socioambiental.org/pages/about/?lang=en).

## Usage

It can be used in various ways:

* Standalone as a single page application.
* As an `<iframe>` element.
* As a jQuery Plugin: [code](https://gitlab.com/socioambiental/mapa/-/blob/master/pages/examples/jquery/index.html),    [demo](https://mapa.eco.br/pages/examples/jquery/).
* As a WebComponent:  [code](https://gitlab.com/socioambiental/mapa/-/blob/master/pages/examples/component/index.html), [demo](https://mapa.eco.br/pages/examples/component/).
* As a [Vue.js](https://vuejs.org) component.

## Porfolio

It's used at:

* [ISA's Conservation Units website](https://uc.socioambiental.org/en) (coverage: Brazil).
* [ISA's Indigenous Lands website](https://ti.socioambiental.org/en) (coverage: Brazil).
* [Terras+](https://terrasmais.eco.br/v1/?lang=en) (coverage: Brazilian Legal Amazon).
* [Alertas+](https://alertas.socioambiental.org/?lang=en) (coverage: Brazilian Legal Amazon).
* [RAISG's Minería Ilegal website](https://mineria.amazoniasocioambiental.org/) (coverage: entire Amazon).

Upcoming:

* [Xingu+](https://xingumais.org.br) (coverage: Xingu Corridor).

## Credits

* Developed at [Instituto Socioambiental](https://www.socioambiental.org).

* Distributed under [GNU GPLv3+ License](./LICENSE).

* Icons:
  * Newer versions use icons from [Ajaxload](http://ajaxload.info/).

  * Previous versions used loading icon from [Preloaders.net](https://preloaders.net/),
    which allows free-of-charge use for non-commercial projects,
    see [terms](https://preloaders.net/en/terms_of_use).
