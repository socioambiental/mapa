const webpack           = require("webpack");
const path              = require('path');
const VueLoaderPlugin   = require('vue-loader/lib/plugin');
const TerserPlugin      = require('terser-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const rules = [
  {
    test: /\.vue$/,
    loader: 'vue-loader',
  },
  {
    test: /\.css$/i,
    use: ['vue-style-loader', 'style-loader', 'css-loader'],
  },
  {
    test: /\.(png|jpg|gif)$/i,
    use: [
      {
        loader: 'url-loader',
        options: {
          //limit: 8192,
        },
      },
    ],
  },
  {
    test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
    use: [
      {
        loader: 'url-loader',
        options: {
          //limit: 8192,
        },
      },
    ],
  },
  {
    test: /\.svg$/i,
    use: [
      {
        loader: 'url-loader',
        options: {
          encoding: false,
        },
      },
    ],
  },
  {
    test: /\.s[ac]ss$/i,
    use: [
      // Creates `style` nodes from JS strings
      'style-loader',
      // Translates CSS into CommonJS
      'css-loader',
      // Compiles Sass to CSS
      'sass-loader',
    ],
  },
  {
    test: /\.less$/,
    use: [
      {
        loader: 'style-loader', // creates style nodes from JS strings
      },
      {
        loader: 'css-loader', // translates CSS into CommonJS
      },
      {
        loader: 'less-loader', // compiles Less to CSS
      },
    ],
  },
  {
    test: /\.thtml$/i,
    loader: 'html-loader',
  },
];

const optimization = {
  minimize : true,
  minimizer: [new TerserPlugin({
    test: /\.min\.js(\?.*)?$/i,
  })],
};

const watchOptions = {
  aggregateTimeout : 300,
  poll             : 1000,
  ignored          : [ /node_modules/ ],
};

const mod = {
  rules: rules,
};

const output = {
  path: path.resolve(__dirname, '../dist/v2/js'),
  filename: '[name].js'
};

const alias = {
  'vue$'     : 'vue/dist/vue.esm.js',
  'vue-i18n$': 'vue-i18n/dist/vue-i18n.esm.js',
};

const resolve = {
  alias: alias,
};

module.exports = [{
  name         : 'main',
  mode         : 'development',
  module       : mod,
  resolve      : resolve,
  optimization : optimization,
  output       : output,
  watchOptions : watchOptions,
  plugins      : [
    new VueLoaderPlugin(),
    // Add jQuery everywhere
    new webpack.ProvidePlugin({
      'jQuery'   : 'jquery',
      //'Vue'    : ['vue/dist/vue.esm.js',        'default'],
      //'VueI18n': ['vue-i18n/dist/vue-i18n.esm', 'default'],
    }),
    new HtmlWebpackPlugin({
      hash      : false,
      title     : 'Xingu+',
      favicon   : './images/icons/xingumais.ico',
      chunks    : [ 'xingumais' ],
      //template: './templates/index.ejs',
      filename  : '../../../custom/xingumais/index.html',
      meta      : {
        'viewport': 'initial-scale=1,maximum-scale=1,user-scalable=no',
        //'charset': 'utf-8',
        //'set-cookie': { 'http-equiv': 'pragma',  content: 'no-cache' },
        //'set-cookie': { 'http-equiv': 'expires', content: '-1' },
      },
    }),
  ],
  entry: {
    //'jquery.isamap'     : './src/map/jquery.js',
    //'jquery.isamap.min' : './src/map/jquery.js',
    'xingumais'           : './src/custom/xingumais.js',
    'xingumais.min'       : './src/custom/xingumais.js',
    'raisg'               : './src/custom/raisg.js',
    'raisg.min'           : './src/custom/raisg.js',
    'web_component'       : './src/map/web_component.js',
    'standalone'          : './src/map/standalone.js',
    'about'               : './src/pages/about.js',
    'examples'            : './src/pages/examples.js',
    'footer'              : './src/pages/footer.js',
    'index'               : './src/pages/index.js',
    'navigation'          : './src/pages/navigation.js',
    'notes'               : './src/pages/notes.js',
  },
}, {
  name         : 'jquery',
  mode         : 'development',
  module       : mod,
  resolve      : resolve,
  optimization : optimization,
  output       : output,
  watchOptions : watchOptions,
  plugins      : [
    new VueLoaderPlugin(),
    // Do not bundle jQuery in the jQuery plugin
    //new webpack.ProvidePlugin({
    //  'jQuery'   : 'jquery',
    //  //'Vue'    : ['vue/dist/vue.esm.js',        'default'],
    //  //'VueI18n': ['vue-i18n/dist/vue-i18n.esm', 'default'],
    //}),
  ],
  entry: {
    'jquery.isamap'     : './src/map/jquery.js',
    'jquery.isamap.min' : './src/map/jquery.js',
  },
}];
