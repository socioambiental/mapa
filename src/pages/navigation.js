'use strict';

import { pages  } from './pages.js';
import { footer } from './footer.js';

export var navigation = new Vue({
  i18n   : pages.i18n,
  el     : '#navigation',
  data   : {
    locale: pages.i18n.locale,
  },
  mounted: function() {
    this.localeTransition();
  },
  methods: {
    localeTransition() {
      var self = this;

      pages.bus.$on('locale-transition', function(value) {
        self.locale = value;
      });
    },
  },
  watch: {
    locale: function(value) {
      footer.localeTransition(value);
    }
  },
  computed: {
    mapUrl: function() {
      return pages.mapUrl + '?lang=' + this.locale;
    },
  },
});
