'use strict';

import { notes } from '../map/notes.js';
import { pages } from './pages.js';

export var note = new Vue({
  i18n: pages.i18n,
  el  : '#note',
  data: {
    items: notes[pages.defaultLocale].items,
  },
  mounted: function() {
    this.localeTransition();
  },
  methods: {
    localeTransition() {
      var self = this;

      pages.bus.$on('locale-transition', function(value) {
        self.items  = notes[value].items;
      });
    },
  },
});
