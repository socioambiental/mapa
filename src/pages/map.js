'use strict';

jQuery(document).ready(function() {
  // Handles map resizing
  function resizeMap() {
    var offset = 22;
    var nav    = jQuery('#navigation').height();
    var win    = jQuery(window).height();
    var height = win - nav - offset;

    jQuery('#map').css('height', height + 'px');
  }

  // Resize when the window is resized
  jQuery(window).on('resize', function() {
    resizeMap();
  });

  // Resize when the document is ready
  resizeMap();
});
