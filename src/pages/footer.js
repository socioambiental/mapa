'use strict';

import { pages             } from './pages.js';
import { getQueryVariable  } from '../common/url_query.js';
import { updateQueryString } from '../common/url_query.js';

export var footer = new Vue({
  i18n   : pages.i18n,
  el     : '#footer',
  data   : {
    locale: pages.defaultLocale,
  },
  mounted: function() {
    this.setLang();
  },
  methods: {
    getQueryVariable: getQueryVariable,
    updateQueryString: updateQueryString,
    setLang: function() {
      var userLang = (navigator.language || navigator.userLanguage).replace(/(-|_).*$/, '');
      var lang     = this.getQueryVariable('lang');

      // If lang is not specified in the URL, try to guess the user language
      // Fallback to the default language
      if (lang == undefined || lang == 'undefined' || lang == 'null') {
        if (pages.i18n.messages[userLang] != undefined) {
          lang = userLang;
        }
        else {
          lang = pages.defaultLocale;
        }
      }

      this.localeTransition(lang);
      //this.setTitle();
    },
    setTitle: function(message = null) {
      // Set title according to a given message
      // Fallback to a string related to the pathname
      // And the fallback to a default message
      var title = pages.i18n.messages[pages.i18n.locale].name;
      var path  = document.location.pathname.split('/');
      var key   = path[path.length -1] != '' ? path[path.length -1] : path[path.length -2];

      if (message != null) {
        title += ' | ' + pages.i18n.messages[pages.i18n.locale][message];
      }
      else if (typeof pages.i18n.messages[pages.i18n.locale][key] != 'undefined') {
        title += ' | ' + pages.i18n.messages[pages.i18n.locale][key];
      }
      else {
        title += ' | ' + pages.i18n.messages[pages.i18n.locale].isamap;
      }

      document.title = title;
    },
    localeTransition: function(value) {
      this.locale       = value;
      var title         = pages.i18n.messages[value].name;
      var url           = this.updateQueryString('lang', value);
      pages.i18n.locale = value;

      // See https://stackoverflow.com/questions/824349/modify-the-url-without-reloading-the-page#3354511
      //     https://stackoverflow.com/questions/2494213/changing-window-location-without-triggering-refresh
      //window.location.href = url;
      window.history.replaceState({}, title, url);

      // Wrap on $nextTick to give time to other apps to register on this event
      this.$nextTick(function() {
        pages.bus.$emit('locale-transition', value);
      });
    },
  },
  computed: {
    footerIframe: function() {
      return 'https://rodape.socioambiental.org/?sponsors=moore&lang=' + this.locale;
    },
  },
});
