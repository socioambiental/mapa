'use strict';

import { messages } from '../map/messages.js';

const defaultLocale = 'pt-br';

// Variables for static pages
export var pages = {
  defaultLocale: defaultLocale,

  i18n: new VueI18n({
    locale  : defaultLocale,
    messages: messages,
  }),

  // Holds application state
  state: {},

  // A bus for intercommunication
  bus: new Vue(),

  mapUrl: 'https://mapa.eco.br/v1/',
}
