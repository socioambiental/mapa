'use strict';

import { pages  } from './pages.js';
import { footer } from './footer.js';

export var about = new Vue({
  i18n   : pages.i18n,
  el     : '#about',
  data   : {
    locale: pages.defaultLocale,
  },
  mounted: function() {
    this.localeTransition();
  },
  methods: {
    localeTransition() {
      var self = this;

      pages.bus.$on('locale-transition', function(value) {
        self.locale = value;
      });
    },
  },
  watch: {
    locale: function(value) {
      footer.localeTransition(value);
    }
  },
});
