// Check variable type
export function varType(value) {
  if (Array.isArray(value)) {
    return Array;
  }
  else if (typeof(value) == 'string') {
    return String;
  }
  else if (typeof(value) == 'number') {
    return Number;
  }
  else if (typeof(value) == 'boolean') {
    return Boolean;
  }
  else if (typeof(value) == 'function') {
    return Function;
  }
  else {
    return Object;
  }
}
