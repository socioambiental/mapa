import Vue                   from 'vue';
import vueCustomElement      from 'vue-custom-element'
import { buildProps        } from './component/props.js';
import { mapComponentMixin } from './component/mixin.js';

// Register plugin
// See https://github.com/karol-f/vue-custom-element
//     https://karol-f.github.io/vue-custom-element/
//     https://www.npmjs.com/package/vue-custom-element
Vue.use(vueCustomElement);

// Setup component
var vm = {
  mixins  : [ mapComponentMixin ],
  props   : {},
  template: '<div class="isamap" v-bind:style="getStyle"></div>',
}

// Add props
vm = buildProps(vm)

// Initialize component
Vue.customElement('mapa-eco', vm, {
  shadow: false,
});
