var globals = require('./globals');

export function loaders() {
  return {
    mapLoadError: function(info) {
      var message = 'Error loading map';
      message     = info != '' ? message + ': ' + info : message;

      jQuery('.isamap').unblock();
      jQuery('.isamap').block({
        message : '<span>'+ message + '</span>',
        css     : globals.blockUIStyle,
      });
    },

    /**
     * Load an isaMap instance, making sure needed resources are available.
     */
    loadMetadata: function(self, baseUrl = '') {
      if (baseUrl == undefined) {
        baseUrl = '';
      }

      jQuery(document).on('isaMapLoadedMetadata', function() {
        // Check if we have all the needed resources.
        if (globals.apiDataLoadStatus.ufs == true &&
            globals.apiDataLoadStatus.tis == true &&
            globals.apiDataLoadStatus.ucs == true
        ) {
          jQuery('.isamap').unblock();
          jQuery(document).trigger('isaMapLoaded');
        }
      });

      // Make sure that ARP metadata is available
      self.getUfMetadata(self, baseUrl);
      self.getIndineousLandsMetadata(self, baseUrl);
      self.getConservationAreasMetadata(self, baseUrl);
    },

    /**
     * Retrieve UF Metadata.
     */
    getUfMetadata: function(self, baseUrl = '') {
      if (Object.getOwnPropertyNames(globals.ufs).length > 0) {
        jQuery(document).trigger('isaMapLoadedMetadata');
        return;
      }

      jQuery.ajax({
        //xhrFields: {
        //  withCredentials: true,
        //},
        //headers: {
        //  'Authorization': 'Basic ' + btoa(globals.apiBasicAuth),
        //},
        //data: {
        //  'tipo': 'uf',
        //},
        //url: "https://api.socioambiental.org/sisarp/v1/listbox.json",
        //url: globals.defaultSettings.baseUrl + "/data/sisarp/v1/ufs.json",
        //url: "/data/sisarp/v1/ufs.json",
        //url: "/data/sisarp/v2/ufs.json",
        url: baseUrl + "/data/sisarp/v2/ufs.json",
        error: function() {
          self.mapLoadError('cannot get UF metadata.');
        },
      }).done(function(result) {
        for (var uf in result.data) {
          globals.ufs[result.data[uf].uf] = result.data[uf].nome;
        }

        globals.apiDataLoadStatus.ufs = true;
        jQuery(document).trigger('isaMapLoadedMetadata');
      });
    },

    /**
     * Retrieve Indigenous Lands Metadata from SisARP API.
     */
    getIndineousLandsMetadata: function(self, baseUrl = '') {
      if (Object.getOwnPropertyNames(globals.arps.tis).length > 0) {
        jQuery(document).trigger('isaMapLoadedMetadata');
        return;
      }

      jQuery.ajax({
        //xhrFields: {
        //  withCredentials: true,
        //},
        //headers: {
        //  'Authorization': 'Basic ' + btoa(globals.apiBasicAuth),
        //},
        //url: "https://api.socioambiental.org/sisarp/v1/terra_indigena.json",
        //url: globals.defaultSettings.baseUrl + "/data/sisarp/v1/tis.json",
        //url: "/data/sisarp/v1/tis.json",
        //url: "/data/sisarp/v2/tis.json",
        url: baseUrl + "/data/sisarp/v2/tis.json",
        error: function() {
          self.mapLoadError('cannot get Indigenous Lands metadata.');
        },
      }).done(function(result) {
        globals.arps.tis              = result.tis;
        globals.arpsByProperty.tis    = result.properties;
        globals.apiDataLoadStatus.tis = true;
        jQuery(document).trigger('isaMapLoadedMetadata');
      });
    },

    /**
     * Retrieve Conservation Units Metadata from SisARP API.
     */
    getConservationAreasMetadata: function(self, baseUrl = '') {
      if (Object.getOwnPropertyNames(globals.arps.ucs).length > 0) {
        jQuery(document).trigger('isaMapLoadedMetadata');
        return;
      }

      jQuery.ajax({
        // Get only confirmed UCs (status = 2)
        //url: "https://api.socioambiental.org/sisarp/v2/uc?status=2&apikey=" + globals.apiKey,
        //url: globals.defaultSettings.baseUrl + "/data/sisarp/v2/ucs.json",
        //url: "/data/sisarp/v2/ucs.json",
        url: baseUrl + "/data/sisarp/v2/ucs.json",
        error: function() {
          self.mapLoadError('cannot get Conservation Areas metadata.');
        },
      }).done(function(result) {
        globals.arps.ucs              = result.ucs;
        globals.arpsByProperty.ucs    = result.properties;
        globals.apiDataLoadStatus.ucs = true;
        jQuery(document).trigger('isaMapLoadedMetadata');
      });

      return;
    },
  }
}
