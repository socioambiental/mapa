//import L from '../../leaflet.js';
import 'chosen-js';
import 'chosen-js/chosen.css';

export default {
  data: function() {
    return {
      // UI
      name        : 'search-control',
      icon        : 'fa-search',
      state       : 'closed',
      collapsed   : true,

      // State
      url         : '',
      code        : '',
      embed       : '',
      groups      : {},
      initialized : false,

      // Standard isaMap properties
      isamap : null,
      locale : null,
      map    : null,
      control: null,
      id     : null,
    }
  },
  mounted: function() {
    this.stopPropagation();
    this.buildSelect();
  },
  computed: {
    containerClass: function() {
      return 'leaflet-control leaflet-small-widget leaflet-bar leaflet-' + this.name + ' ' + this.state;
    },
    controlClass: function() {
      return 'leaflet-small-widget-toggle ' + this.state;
    },
    iconClass: function() {
      return 'fa ' + this.icon;
    },
    selectId: function() {
      return this.isamap.settings.mapId + '-leaflet-arp-selection';
    },
  },
  methods: {
    stopPropagation: function() {
      L.DomEvent.disableScrollPropagation(this.$el);
      L.DomEvent.disableClickPropagation(this.$el);
      L.DomEvent.on(this.$el, 'wheel', L.DomEvent.stopPropagation);
      L.DomEvent.on(this.$el, 'click', L.DomEvent.stopPropagation);
    },
    toggleControl: function() {
      if (this.state == 'closed') {
        this.state = 'opened';
      }
      else {
        this.state = 'closed';
      }

      // Give time in case the select still need to be built by this.buildSelect()
      this.$nextTick(function() {
        this.chosen();
      });
    },
    toggleItem: function(event) {
      jQuery(event.target).parent().toggleClass('closed');
      jQuery(event.target).parent().siblings('.legend-elements').toggleClass('closed');
    },
    isSelected: function(item) {
      if (item.selected != undefined) {
        return item.selected;
      }

      return false;
    },
    buildSelect: function() {
      // Globals
      var globals = this.isamap.methods.getGlobals();

      // TIs by name: group
      this.groups.ti           = {};
      this.groups.ti.items     = {};
      this.groups.ti.translate = false;

      // Build sorted lists for select boxes
      var selects = { 'tis': [], 'ucs': [] };
      selects.tis = this.isamap.methods.sortArpsByName(globals.arps.tis);
      selects.ucs = this.isamap.methods.sortArpsByName(globals.arps.ucs);

      // TIs by name: items
      for (var i in selects.tis) {
        this.groups.ti.items[i]         = selects.tis[i];
        this.groups.ti.items[i].caption = selects.tis[i].categoria + ' ' + this.isamap.methods.truncateString(selects.tis[i].nome_arp, 45);

        // Select ARPs currently present on this.isamap.settings.arps
        if (this.isamap.settings.arps != null && this.isamap.settings.arps.indexOf(selects.tis[i].id.toString()) != -1) {
          this.groups.ti.items[i].selected = true;
        }
      }

      // UCs by name: group
      this.groups.uc           = {};
      this.groups.uc.items     = {};
      this.groups.uc.translate = false;
      this.groups.ti.label     = 'indigenous_lands';
      this.groups.uc.label     = 'conservation_areas';

      // UCs by name: items
      for (var i in selects.ucs) {
        this.groups.uc.items[i]         = selects.ucs[i];
        this.groups.uc.items[i].caption = this.isamap.methods.truncateString(selects.ucs[i].categoria + ' ' + selects.ucs[i].nome_arp, 45);

        // Selected those ARPs that are present on this.isamap.settings.arps
        if (this.isamap.settings.arps != null && this.isamap.settings.arps.indexOf(selects.ucs[i].id.toString()) != -1) {
          this.groups.uc.items[i].selected = true;
        }
      }

      // ARP Groups
      for (var group in globals.arps) {
        for (var property in globals.arpsProperties[group]) {
          var currentProperty                  = globals.arpsProperties[group][property];
          var groupProperty                    = group + '.' + currentProperty;
          this.groups[groupProperty]           = {};
          this.groups[groupProperty].items     = {};
          this.groups[groupProperty].label     = groupProperty;
          this.groups[groupProperty].translate = true;

          for (var propertyItems in globals.arpsByProperty[group][currentProperty]) {
            var currentIndex                                  = group + '.' + propertyItems;
            this.groups[groupProperty].items[currentIndex]    = {};
            this.groups[groupProperty].items[currentIndex].id = groupProperty + '.' + propertyItems;

            // Exception handling
            if (group == 'tis') {
              if (currentProperty == 'uf') {
                this.groups[groupProperty].items[currentIndex].caption = globals.ufs[propertyItems] + ' - ' + propertyItems;
              }
              else if (currentProperty == 'populacao') {
                this.groups[groupProperty].items[currentIndex].caption = 'population.' + propertyItems;
              }
              else if (currentProperty == 'area') {
                this.groups[groupProperty].items[currentIndex].caption = 'area.' + propertyItems;
              }
              else {
                if (propertyItems != null && propertyItems != undefined && propertyItems != '') {
                  this.groups[groupProperty].items[currentIndex].caption = propertyItems;
                }
                else {
                  this.groups[groupProperty].items[currentIndex].caption = 'no_info';
                }
              }
            }
            else {
              if (propertyItems != null && propertyItems != undefined && propertyItems != '') {
                this.groups[groupProperty].items[currentIndex].caption = propertyItems;
              }
              else {
                this.groups[groupProperty].items[currentIndex].caption = 'no_info';
              }
            }

            this.groups[groupProperty].items[currentIndex].length = ' (' + globals.arpsByProperty[group][currentProperty][propertyItems].length + ')';

            // Select this.groups currently present on this.isamap.settings.arps
            if (this.isamap.settings.layers != null && this.isamap.settings.layers.indexOf(groupProperty + '.' + propertyItems) != -1) {
              this.groups[groupProperty].items[currentIndex].selected = true;
            }
          }
        }
      }
    },
    clearSelection: function() {
      // Thanks https://stackoverflow.com/questions/11365212/how-do-i-reset-a-jquery-chosen-select-option-with-jquery#11365340
      jQuery('#' + this.isamap.settings.mapId + '-leaflet-arp-selection').val('').trigger('chosen:updated');
      jQuery('#' + this.isamap.settings.mapId + '-leaflet-arp-selection').trigger('chosen:open');

      this.isamap.methods.removeArpsByGroup();
      this.isamap.methods.restoreDefaultBounds();
    },
    chosenTrigger: function() {
      //if (this.initialized == true) {
      //  // Keep chosen updated
      //  //this.$nextTick(function() {
      //    jQuery('#' + this.selectId).trigger('chosen:updated');
      //  //});
      //
      //  return;
      //}

      // Activate chosen on toggle
      //jQuery('#' + this.isamap.settings.mapId + '-leaflet-arp-selection .chosen-search-input').focus();
      //jQuery('#' + this.isamap.settings.mapId + '-leaflet-arp-selection .chosen-search-input').text('');
      //jQuery('#' + this.selectId).trigger('chosen:activate');
      jQuery('#' + this.selectId).trigger('chosen:open');
    },
    chosen: function() {
      if (this.state == 'closed') {
        return;
      }

      var self = this;

      if (this.initialized == false) {
        jQuery('#' + this.selectId).chosen({
          'search_contains'                 : true,
          'include_group_label_in_selected' : true,
          'no_results_text'                 : self.isamap.methods.message('no_results'),
        });

        self.chosenChange();

        self.initialized = true;
      }

      self.chosenTrigger();
    },
    chosenChange: function() {
      var self    = this;
      var globals = this.isamap.methods.getGlobals();

      jQuery('#' + this.selectId).change(function() {
        var ids          = [];
        var showingGroup = false;

        // Remove all ARPs
        //self.isamap.methods.removeArpsById();

        // Remove all ARP group layers
        self.isamap.methods.removeArpsByGroup();

        if (this.multiple == false) {
          ids.push(this.value);
        }
        else {
          for (var i in this.selectedOptions) {
            if (this.selectedOptions[i].value != undefined) {
              var value = this.selectedOptions[i].value;

              if (parseInt(value)) {
                ids.push(value);
              }
              else {
                var split    = value.split('.');
                var subgroup = split[0];
                split.splice(0, 1);
                var layer    = split.join('.');
                showingGroup = true;

                self.isamap.methods.showLayer(subgroup, layer);
              }
            }
          }
        }

        // Make sure chosen is updated: it was observed that if chosen-drop element has
        // position set other than "absolute" then some elements might not be updated.
        jQuery('#' + this.selectId).trigger('chosen:updated');

        // Do not work on empty selection
        if (ids.length != 0) {
          // Due to performance concerns, do not plot limits if there are too many selected ARPs
          if (ids.length <= globals.maxArpsWithLimits) {
            self.isamap.methods.addArpsById({ arps: ids, type: 'markers', show: true, fitBounds: true });
            self.isamap.methods.addArpsById({ arps: ids, type: 'limits',  show: true });
          }
          else {
            self.isamap.methods.addArpsById({ arps: ids, type: 'markers', show: true });
          }
        }
        else if (showingGroup == false) {
          // If there are no remaining ARPs, set zoom and center to initial settings
          self.isamap.methods.restoreDefaultBounds();
        }
      });
    },
  },
}
