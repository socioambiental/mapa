//import L from '../../leaflet.js';

export default {
  i18n: {},
  data: function() {
    return {
      // Standard isaMap properties
      isamap : null,
      locale : null,
      map    : null,
      control: null,
      id     : null,
    }
  },
  mounted: function() {
    this.stopPropagation();
    this.localeTransition();
  },
  methods: {
    stopPropagation: function() {
      L.DomEvent.disableScrollPropagation(this.$el);
      L.DomEvent.disableClickPropagation(this.$el);
      L.DomEvent.on(this.$el, 'wheel', L.DomEvent.stopPropagation);
      L.DomEvent.on(this.$el, 'click', L.DomEvent.stopPropagation);
    },
    localeTransition() {
      var self = this;

      this.isamap.bus.$on('locale-transition', function(value) {
        self.locale = value;
      });
    },
  },
  watch: {
    locale: function(value) {
      this.isamap.methods.localeTransition(value);
    }
  },
}
