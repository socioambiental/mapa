//import L from '../../leaflet.js';

export default {
  i18n: {},
  data: function() {
    return {
      options  : {},
      style    : null,

      // Standard isaMap properties
      isamap   : null,
      locale   : null,
      map      : null,
      control  : null,
      id       : null,
    }
  },
  mounted: function() {
    this.stopPropagation();
  },
  methods: {
    stopPropagation: function() {
      L.DomEvent.disableScrollPropagation(this.$el);
      L.DomEvent.disableClickPropagation(this.$el);
      L.DomEvent.on(this.$el, 'wheel', L.DomEvent.stopPropagation);
      L.DomEvent.on(this.$el, 'click', L.DomEvent.stopPropagation);
    },
  },
}
