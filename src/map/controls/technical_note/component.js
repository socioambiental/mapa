//import L       from '../../leaflet.js';
import { notes } from '../../notes.js';

export default {
  data: function() {
    return {
      // UI
      name     : 'technical-note',
      icon     : 'fa-info-circle',
      state    : 'closed',
      collapsed: true,

      // Options
      items    : { },

      // Standard isaMap properties
      isamap : null,
      locale : null,
      map    : null,
      control: null,
      id     : null,
    }
  },
  mounted: function() {
    this.stopPropagation();
    this.localeTransition();

    this.items = notes[this.isamap.settings.lang].items;
  },
  computed: {
    containerClass: function() {
      return 'leaflet-control leaflet-small-widget leaflet-bar leaflet-' + this.name + ' ' + this.state;
    },
    controlClass: function() {
      return 'leaflet-small-widget-toggle ' + this.state;
    },
    iconClass: function() {
      return 'fa ' + this.icon;
    },
    titleClass: function() {
      return this.collapsed == true ? 'legend-title closed' : 'legend-title';
    },
    pageUrl: function() {
      return this.isamap.settings.pagesUrl + 'note/?lang=' + this.isamap.settings.lang;
    },
  },
  methods: {
    stopPropagation: function() {
      L.DomEvent.disableScrollPropagation(this.$el);
      L.DomEvent.disableClickPropagation(this.$el);
      L.DomEvent.on(this.$el, 'wheel', L.DomEvent.stopPropagation);
      L.DomEvent.on(this.$el, 'click', L.DomEvent.stopPropagation);
    },
    localeTransition() {
      var self = this;

      self.isamap.bus.$on('locale-transition', function(value) {
        self.locale = value;
        self.items  = notes[value].items;
      });
    },
    toggleControl: function() {
      if (this.state == 'closed') {
        this.state = 'opened';
      }
      else {
        this.state = 'closed';
      }
    },
    toggleItem: function(event) {
      jQuery(event.target).parent().toggleClass('closed');
      jQuery(event.target).parent().siblings('.legend-elements').toggleClass('closed');
    },
  },
}
