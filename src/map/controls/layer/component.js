//import L from '../../leaflet.js';
import 'ion-rangeslider';
import EventBus from '../../../utils/EventBus'

export default {
  data: function() {
    return {
      // UI
      name     : 'layer',
      icon     : 'styled-layer-icon',
      state    : 'opened',
      collapsed: true,

      // Layers
      base     : { },
      layers   : { },
      
      // State
      sliders  : { },
      states   : { },
      change   : 0,
      options  : { },

      // Standard isaMap properties
      isamap : null,
      locale : null,
      map    : null,
      control: null,
      id     : null,
    }
  },
  mounted: function() {
    const self = this;
    this.stopPropagation();
    this.localeTransition();
    this.layerStates();
    this.mapListener();
    this.addSliders();
    EventBus.$on('state-box-layers', function (value = true) {
      self.state = value ? 'opened' : 'closed';
    });
  },
  computed: {
    containerClass: function() {
      return 'leaflet-control leaflet-control-layers' + (this.state == 'opened' ? ' leaflet-control-layers-expanded' : '');
    },
    sectionClass: function() {
      return 'ac-container leaflet-control-layers-list';
    },
    toggleClass: function() {
      return 'leaflet-control-layers-toggle';
    },
    formId: function() {
      return 'ac-form-' + this.id;
    },
  },
  methods: {
    stopPropagation: function() {
      L.DomEvent.disableScrollPropagation(this.$el);
      L.DomEvent.disableClickPropagation(this.$el);
      L.DomEvent.on(this.$el, 'wheel', L.DomEvent.stopPropagation);
      L.DomEvent.on(this.$el, 'click', L.DomEvent.stopPropagation);
    },
    localeTransition() {
      var self = this;

      self.isamap.bus.$on('locale-transition', function(value) {
        self.locale = value;
      });
    },
    toggleControl: function() {
      if (this.state == 'closed') {
        this.state = 'opened';
      }
      else {
        this.state = 'closed';
      }
    },
    layerId: function(data) {
      //return this.aid + '-ac-' + data.description.replace(/ /g,'');
      return this.id + '-ac-' + L.Util.stamp(data.layer);
    },
    accordionId: function(name) {
      return this.id + '-ac-' + name;
    },
    sliderId: function(name) {
      return this.id + '-sl-' + name;
    },
    toggleLayer: function(group, layer) {
      this.isamap.methods.toggleLayer(group, layer);
    },
    activateBaseLayer: function(group, layer) {
      this.isamap.methods.activateBaseLayer(group, layer);
    },
    checkLayer: function(group, layer) {
      return this.isamap.methods.hasLayer(group, layer) ? 'checked' : '';
    },
    checkBaseLayer: function(group, layer) {
      return this.isamap.methods.hasBaseLayer(group, layer) ? 'checked' : '';
    },
    layerStates: function() {
      var sets = [ 'base', 'layers' ];

      for (var set in sets) {
        for (var group in this[sets[set]]) {
          for (var layer in this[sets[set]][group].layers) {
            var id                                  = L.Util.stamp(this[sets[set]][group].layers[layer].layer);
            this[sets[set]][group].layers[layer].id = id;

            if (sets[set] == 'base') {
              this.states[id] = this.checkBaseLayer(group, layer);
            }
            else {
              this.states[id] = this.checkLayer(group, layer);
            }
          }
        }
      }
    },
    mapListener() {
      var self = this;

      // See https://stackoverflow.com/questions/32106155/can-you-force-vue-js-to-reload-re-render#40586872
      self.isamap.map.on('layeradd', function(e) {
        //self.$forceUpdate();
        var id               = L.Util.stamp(e.layer);
        self.states[id] = 'checked';
        self.change++;

        // Update slider
        if (e.layer.slider != undefined) {
          if (e.layer.slider.type == 'single') {
            // Initialize tracked values
            if (self.sliders[e.layer.slider].initialized == undefined) {
              if (self.sliders[e.layer.slider].from == undefined ||
                self.sliders[e.layer.slider].from == 0) {
                self.sliders[e.layer.slider].from = e.layer.sliderValue;
              }

              self.sliders[e.layer.slider].initialized = true;
            }
            else {
              if (self.sliders[e.layer.slider].from == undefined) {
                self.sliders[e.layer.slider].from = 0;
              }
            }

            if (self.sliders[e.layer.slider].instance.result.from != sliderValue) {
              self.sliders[e.layer.slider].instance.update({
                from: e.layer.sliderValue,
              });
            }
          }
          else {
            // Initialize tracked values
            if (self.sliders[e.layer.slider].initialized == undefined) {
              if (self.sliders[e.layer.slider].from == undefined ||
                self.sliders[e.layer.slider].from == 0) {
                self.sliders[e.layer.slider].from = e.layer.sliderValue;
              }

              if (self.sliders[e.layer.slider].to == undefined ||
                self.sliders[e.layer.slider].to == 0) {
                self.sliders[e.layer.slider].to = e.layer.sliderValue;
              }

              self.sliders[e.layer.slider].initialized = true;
            }
            else {
              if (self.sliders[e.layer.slider].from == undefined) {
                self.sliders[e.layer.slider].from = 0;
              }

              if (self.sliders[e.layer.slider].to == undefined) {
                self.sliders[e.layer.slider].to = 0;
              }
            }

            if (e.layer.sliderValue >= self.sliders[e.layer.slider].to) {
              self.sliders[e.layer.slider].to = e.layer.sliderValue;
            }
            else if (e.layer.sliderValue <= self.sliders[e.layer.slider].from) {
              self.sliders[e.layer.slider].from = e.layer.sliderValue;
            }

            //self.methods.debug('Update slider - value: ' + e.layer.sliderValue);
            //self.methods.debug('Update slider - from : ' + self.sliders[e.layer.slider].from);
            //self.methods.debug('Update slider - to   : ' + self.sliders[e.layer.slider].to);

            // Update the slider only if needed
            if (self.sliders[e.layer.slider].instance.result.from != self.sliders[e.layer.slider].from ||
              self.sliders[e.layer.slider].instance.result.to     != self.sliders[e.layer.slider].to
            ) {
              //this.$nextTick(function() {
              self.sliders[e.layer.slider].instance.update({
                from: self.sliders[e.layer.slider].from,
                to  : self.sliders[e.layer.slider].to,
              });
              //});
            }
          }
        }
      }, this);

      self.isamap.map.on('layerremove', function(e) {
        //self.$forceUpdate();
        var id          = L.Util.stamp(e.layer);
        self.states[id] = '';
        self.change++;
      }, this);
    },
    changeSingleSlider: function(data) {
      var self       = this;
      var id         = data.input.attr('id');
      var from_value = 0;
      var index;

      self.isamap.methods.debug('Activating item ' + data.from_value + ' of single slider ' + id + '...');

      // First hide all other layers from the group
      for (var group in self.layers) {
        if (self.layers[group].slider != undefined && self.layers[group].sliderId == id) {
          index = -1;

          // The new value
          if (data.from_value != 0) {
            from_value = this.layers[group].sliderSort[data.from_value];
          }

          // Track latest from value
          self.sliders[group].from = from_value;

          for (var layer in self.layers[group].layers) {
            // The layer we want to check for displayal
            var value = this.layers[group].sliderSort[self.layers[group].layers[layer].description];

            if (value == from_value) {
              index = layer;
            }
            else {
              // Hide the other layers
              layer = self.layers[group].layers[layer].layer;

              if (self.isamap.map.hasLayer(layer)) {
                self.isamap.map.removeLayer(layer);
              }
            }
          }

          // Add the selected layer
          if (index != -1 && !self.isamap.map.hasLayer(self.layers[group].layers[index].layer)) {
            self.isamap.map.addLayer(self.layers[group].layers[index].layer);
          }

          // We found what we wanted
          break;
        }
      }
    },
    changeDoubleSlider: function(data) {
      var self       = this;
      var from_value = 0;
      var to_value   = 0;
      var id         = data.input.attr('id');

      self.isamap.methods.debug('Activating itens from ' + data.from_value + ' to ' + data.to_value + ' of double slider ' + id + '...');

      for (var group in self.layers) {
        if (self.layers[group].slider != undefined && self.layers[group].sliderId == id) {
          //self.isamap.methods.debug("From " + data.from_value);
          //self.isamap.methods.debug("To " + data.to_value);

          // The new value
          if (data.from_value != 0) {
            from_value = this.layers[group].sliderSort[data.from_value];
          }

          if (data.to_value != 0) {
            to_value = this.layers[group].sliderSort[data.to_value];
          }

          // Track latest to and from values
          self.sliders[group].from = from_value;
          self.sliders[group].to   = to_value;

          for (var layer in self.layers[group].layers) {
            //self.isamap.methods.debug(self.layers[group].layers[layer].description);
            //self.isamap.methods.debug(Number(self.layers[group].layers[layer].description));

            // The layer we want to check for displayal
            var value = this.layers[group].sliderSort[self.layers[group].layers[layer].description];

            if (value >= from_value && value <= to_value) {
              self.isamap.methods.debug('Adding layer ' + layer);
              self.isamap.map.addLayer(self.layers[group].layers[layer].layer);
            }
            else {
              self.isamap.methods.debug('Removing layer ' + layer);
              self.isamap.map.removeLayer(self.layers[group].layers[layer].layer);
            }
          }

          // We found what we wanted
          break;
        }
      }
    },
    addSliders: function() {
      var self         = this;
      var sliderValues = { };

      for (var group in this.layers) {
        if (this.layers[group].slider) {
          // See http://ionden.com/a/plugins/ion.rangeSlider/api.html
          sliderValues[group]            = [];
          var sliderValue                = 0;
          var sliderId                   = this.sliderId(group);
          this.layers[group].sliderId    = sliderId;
          this.layers[group].sliderSort  = {};
          this.sliders[group]            = this.layers[group].slider;
          this.sliders[group].sliderId   = sliderId,
            this.sliders[group].skin     = 'isamap';

          // Use onFinish instead of onChange: wait user choise before loading any layer
          this.sliders[group].onFinish = this.layers[group].slider.type == 'single' ? this.changeSingleSlider : this.changeDoubleSlider;

          // Build slider values and tag layers
          for (var layer in this.layers[group].layers) {
            var description                                    = this.layers[group].layers[layer].description
            this.layers[group].layers[layer].layer.slider      = group;
            this.layers[group].layers[layer].layer.sliderValue = sliderValue;
            this.layers[group].sliderSort[description]         = sliderValue ;
            sliderValues[group][sliderValue]                   = description;
            sliderValue++;
          }

          // Apply slider values
          this.sliders[group].values = sliderValues[group];

          // Initialize slider
          jQuery('#' + sliderId).ionRangeSlider(this.sliders[group]);

          // Save instance
          this.sliders[group].instance = jQuery('#' + sliderId).data('ionRangeSlider');
        }
      }
    },
    /*
    sliderPrev: function(group) {
      var instance = this.sliders[group].instance;
      var min      = instance.result.min;
      var from     = instance.result.from;

      if (from > min) {
        instance.update({
          from: from -1,
        });
      }

      this.changeSingleSlider(instance.result);
    },
    sliderNext: function(group) {
      var instance = this.sliders[group].instance;
      var max      = instance.result.max;
      var from     = instance.result.from;

      if (from < max) {
        instance.update({
          from: from + 1,
        });
      }

      if (from + 1 == max) {
        this.sliderPause(group);
      }

      this.changeSingleSlider(instance.result);
    },
    sliderPlay: function(group) {
      if (this.sliders[group].play == undefined) {
        var self                 = this;
        this.sliders[group].play = window.setInterval(self.sliderNext, 3000, group);
      }
    },
    sliderPause: function(group) {
      if (this.sliders[group].play != undefined) {
        window.clearInterval(this.sliders[group].play);
        delete this.sliders[group].play;
      }
    },
    */
  },
}
