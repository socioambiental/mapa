// Localized tecnical notes
export var notes = {
  'en': {
    "items": [
      {
        "title": "Indigenous Lands (ILs)",
        "source": "Instituto Socioambiental (ISA), Protected Areas Monitoring Program and Geoprocessing Laboratory",
        "scale": "1:250,000 and 1:1,000,000",
        "date": "daily update",
        "description": "<p>Daily monitoring of the Federal Official Gazette and the Official Gazettes of the states of Legal Amazonia (with the exception of Amapá, which did not provide internet access to its Official Gazette and the daily monitoring began in December 2016) by the Protected Areas Monitoring Program allows the team to monitor newly created and/or recognized protected areas (PAs) and alterations made to the boundaries of existing areas.</p><p>Based on the descriptive memorandum contained in the decrees (CU) and ministerial directives (IL) creating and recognizing the areas, which include technical information describing the boundaries of the PAs using geographic coordinates and administrative borders, the team plots these boundaries using a cartographic base of 1:250,000 in the case of CUs and ILs in Amazonia, and 1:1,000,000 for the remainder of the country.</p><p>The bases used are SIVAM 1:250,000 for Amazonia and ISA’s base of 1:1,000,000 for the rest of Brazil. Daily monitoring of official publications also allows the team to keep track of administrative acts related to conservation units, such as approval of administrative instruments (management plans, administration plans, public use plans and so on), investigations, the creation, proposal and approval of the regulations of the administrative councils, actions relating to regularization of land ownership, such as compulsory purchases, compensations for Legal Reserves and the Grant for Real Right of Use (CDRU), among others.</p><p>Given that official bulletins are made available on the internet at different moments in time and do not always include previous editions, allowing the corresponding information to be validated and complemented, a consultation was undertaken with the relevant administrative bodies (OEMAS and ICMBio) in August 2010, incorporating further information.</p>"
      },
      {
        "title": "Conservation Units (CUs) and Indigenous Lands (ILs)",
        "source": "Instituto Socioambiental (ISA), Protected Areas Monitoring Program and Geoprocessing Laboratory",
        "scale": "1:250,000 and 1:1,000,000",
        "date": "daily update",
        "description": "<p>Daily monitoring of the Federal Official Gazette and the Official Gazettes of the states of Legal Amazonia (with the exception of Amapá, which did not provide internet access to its Official Gazette and the daily monitoring began in December 2016) by the Protected Areas Monitoring Program allows the team to monitor newly created and/or recognized protected areas (PAs) and alterations made to the boundaries of existing areas.</p><p>Based on the descriptive memorandum contained in the decrees (CU) and ministerial directives (IL) creating and recognizing the areas, which include technical information describing the boundaries of the PAs using geographic coordinates and administrative borders, the team plots these boundaries using a cartographic base of 1:250,000 in the case of CUs and ILs in Amazonia, and 1:1,000,000 for the remainder of the country.</p><p>The bases used are SIVAM 1:250,000 for Amazonia and ISA’s base of 1:1,000,000 for the rest of Brazil. Daily monitoring of official publications also allows the team to keep track of administrative acts related to conservation units, such as approval of administrative instruments (management plans, administration plans, public use plans and so on), investigations, the creation, proposal and approval of the regulations of the administrative councils, actions relating to regularization of land ownership, such as compulsory purchases, compensations for Legal Reserves and the Grant for Real Right of Use (CDRU), among others.</p><p>Given that official bulletins are made available on the internet at different moments in time and do not always include previous editions, allowing the corresponding information to be validated and complemented, a consultation was undertaken with the relevant administrative bodies (OEMAS and ICMBio) in August 2010, incorporating further information.</p>"
      },
      {
        "title": "Biomes and Phytophysionomies",
        "source": "Brazilian Institute of Geography and Statistics (IBGE), linked to the Ministry of Planning, Budget and Management",
        "scale": "1:5,000,000",
        "date": "September 2010",
        "description": "Clusters: Owing to the large number of different kinds of contact between phytophysionomies, all have been grouped into a single class denominated ‘contacts’ where displayed on the web page maps."
      },
      {
        "title": "River Basins",
        "source": "National Water Agency (ANA), linked to the Ministry of the Environment (MMA)",
        "scale": "1:1,000,000",
        "date": "",
        "description": "September 2010"
      },
      {
        "title": "Deforestation",
        "source": "PRODES (Satellite Monitoring of Brazilian Amazonian Rainforest Project) at the National Institute of Spatial Research (INPE), linked to the Ministry of Science and Technology (MCT)",
        "scale": "Thematic image classified in accordance with the key used by the PRODES Digital Project in the base representation (60m x 60m resolution (*) expressed in decimal degrees). This data was elaborated by combining all the classified individual scenes that make up the region of Amazonia in a single thematic map.",
        "date": "October 2014, using data compiled since 1997",
        "description": ""
      },
      {
        "title": "Fires",
        "source": "National Institute of Spatial Research (INPE), linked to the Ministry of Science, Technology and Innovation (MCTI)",
        "scale": "",
        "date": "Daily update, the fires recorded the previous day always being shown",
        "description": "<p>Coverage area of spot: A heat spot indicates the existence of fire in a single picture element (pixel), which varies from 1 km x 1 km to 5 km x 4 km.  This pixel may contain one or more distinct heat spots, although only one spot is indicated. If the fire is extensive, it will be seen in some of the adjacent pixels: in other words, various outbreaks are associated with a single large fires. Satellite and channels available: using all the satellites with optical sensors operating in thermal spectral range – 4um average – whose signal can be received by INPE.</p><p>At the moment (July 2007) AVHRR data is processed from the polar orbit satellites NOAA-15, NOAA-16, NOAA-17, NOAA-18 and NOAA-19, along with MODIS images from the polar orbit satellites NASA TERRA and AQUA, and images from the geostationary satellites GOES-12 and MSG-2.</p><p>Each polar orbit satellite produces at least one set of images per day, while the geostationary satellites can generate several images per hour, meaning that INPE processes a total of more than 100 images per day specifically to detect heat spots. Data is received at the Cachoeira Paulista station in São Paulo and the Cuiabá station in Mato Grosso.<p>"
      },
      {
        "title": "Mining titles",
        "source": "Department of Mineral Production (DNPM), linked to Ministry of Mines and Energy",
        "scale": "",
        "date": "04/01/2016",
        "description": "<p><strong>Clusters Phase</strong></p><ol><li>Interest in research: request for research authorization</li><li>Under research or availability: research authorization; availability</li><li>Authorization for the extraction: extraction request; prospecting request; licensing request; extraction registration request</span></li><li>4. Under extraction: extraction concession; prospecting; licensing; extraction registration</li>"
      },
      {
        "title": "Energy",
        "source": "Agency of Electrical Energy (ANEEL), linked to the Ministry of Mines and Energy (MME)",
        "scale": "n/a",
        "date": "02/02/2016",
        "description": "Grouped by type (PCH – Small Hydroelectric Plant, UHE – Hydroelectric Plant, UTE – Thermoelectric Plant) and by phase (operation, construction, concession and planning)"
      },
      {
        "title": "Caves",
        "source": "Database of the National Cave Investigation and Conservation Centre (CECAV) of the Chico Mendes Institute for Biodiversity Conservation (ICMBio), linked to the Ministry of the Environment (MMA)",
        "scale": "",
        "date": "31/12/2015",
        "description": "<p>In 2004, CECAV created its database on the locations of Brazil’s caves, which receives a permanent inflow of information from other databases, speleological studies, bibliographic material and especially fieldwork conducted by its technicians and environmental analysts.</p><p>The data does not represent all the caves existing in Brazil. It includes only the small portion of caves already explored by individuals, groups or institutions, the results of which have been published in various mediums of communication, and systemized, georeferenced and analyzed by CECAV. Since 2005, CECAV has provided online access to part of this database, including the data either validated by its technical team or showing basic levels of reliability, obtained from trustworthy sources, cited by more than one sources or whose geospatialization matches the descriptions accompanying the data.</p><p>Currently the CECAV database contains around 17,000 records, relating to more than 7,000 caverns. However we know that this data does not reflect the true extent of cave systems in Brazil.  Hence CECAV is conducting an inventory of Brazil’s natural subterranean caves.<br />Geospatialized caves based on the integration of data coming from: field surveys conducted by the CECAV technical team; studies and research submitted to CECAV/Chico Mendes Institute for Biodiversity Conservation; specialized bibliography; CNC – Brazilian National Cave Register, Brazilian Speleology Society SBE; CODEX – National Cave Register, REDESPELEO BRASIL. (Source: CECAV/ICMBio, September 2010)</p><p>"
      },
      /*
        {
          "title": "News",
          "source": "Diverse, specified in each case",
          "scale": "",
          "date": "daily update",
          "description": "One of the daily activities of the Protected Areas Monitoring Program involves indexing related news reports through an active search in primary federal and state sources, principally from the states of Legal Amazonia.  Around 50 sources are monitored weekly and approximately 30 daily. As well as news reports directly related to specific conservation units available on this site, the Program also indexes those referring to correlated themes, such as Indigenous Lands and Peoples, socioenvironmental policies, forestry policy, pressures and threats on protected areas, local initiatives, and so on.  Currently the news database covers more than 100,000 news items."
        },
        */
      {
        "title": "Ramsar sites",
        "source": "Ramsar Sites Information Service (RSIS)",
        "scale": "",
        "date": "May 2010",
        "description": ""
      }
    ]
  },
  'es': {
    "items": [
      {
        "title": "Unidades de Conservación (UCs) y Tierras Indígenas (TIs)",
        "source": "Instituto Socioambiental (ISA), Programa de Monitoreo de Áreas Protegidas y Laboratorio de Geoprocesamiento",
        "scale": "1:250.000",
        "date": "actualización diaria",
        "description": "<p>El seguimiento cotidiano del Diario Oficial de la Unión y de los Diarios Oficiales de los Estados de la Amazonía Legal (con la excepción de Amapá, que no tenía Diario Oficial disponible en la Internet y el seguimiento diario comenzó en diciembre el año 2016) por el equipo del Programa de Monitoreo de Áreas Protegidas permite la supervisión de nuevas áreas protegidas (APs) creadas y/o reconocidas y de los cambios en los límites de las ya existentes.</p><p>A partir del memorial Descriptivo presente en los decretos de creación (UC) y homologación (TI), la información técnica que describe los límites de la APs por medio de coordenadas geográficas, referencias geográficas y límites administrativos, se realiza el trabajo de estos datos con el plotter. Lo cual se hace sobre una base cartográfica de 1:250.000 (en el caso de las UCs y TIs de la Amazonía) y 1:1 millón para el resto del país.</p><p>El procedimiento con el plotter consiste en el caso de las áreas protegidas en relacionar las informaciones descritas con aquellas identificadas en la base cartográfica. Para las áreas protegidas en la Amazonía Legal, la base empleada es la SIVAM en 1:250.000 y para el resto de Brasil, la base utilizada es la del ISA en 1:000.000.</p><p>El monitoreo diario de las publicaciones oficiales permite también el seguimiento de las acciones de gestión relacionadas con las unidades de conservación, tales como la aprobación de instrumentos de gestión (planes de manejo, planes de gestión, uso público y otros), investigaciones, creación, nombramiento y aprobación de los reglamentos de los consejos gestores, acciones relativas a la regularización de la tenencia de la tierra, como expropiaciones, compensaciones de Reservas Legales y la concesión de derecho real de uso (CDRU), entre otros. Dado que la disponibilidad de los boletines oficiales en la Internet se produjo en diferentes momentos a lo largo del tiempo y no siempre se incluyen las ediciones anteriores, en un esfuerzo por validar, detallar y complementar las informaciones correspondientes, se llevó a cabo una consulta a todos los órganos de gestión (OEMAS y ICMBio) en agosto de 2010, incorporando tales datos.</p>"
      },
      {
        "title": "Biomas y Fitofisonomías",
        "source": "Instituto Brasileño de Geografía y Estadística (IBGE), vinculado al Ministerio de Planificación, Presupuesto y Gestión.",
        "scale": "1:5.000.000",
        "date": "Septiembre 2010",
        "description": "Clases de agrupamientos</strong>: Debido a la gran cantidad de tipos de contactos entre las fitofisonomías, se agrupó a todos en una clase única denominada 'contactos', cuando aparecen en los mapas de la página web."
      },
      {
        "title": "Cuencas Hidrográficas",
        "source": "Agencia Nacional de Aguas (ANA), vinculado al Ministerio del Medio Ambiente (MMA)",
        "scale": "1:1.000.000",
        "date": "Septiembre 2010",
        "description": "Clases de agrupamientos: cuencas hidrográficas de nivel 1 y 2"
      },
      {
        "title": "Deforestación",
        "source": "PRODES (Proyecto PRODES Monitoreo de la Selva Amazónica Brasileña por Satélite) - Instituto Nacional de Pesquisas Espaciales (INPE), vinculado al Ministerio de Ciencia y Tecnología (MCT)",
        "scale": "Imagen temática clasificada según la leyenda del proyecto PRODES Digital en la representación matriz (resolución de 60m x 60m (*) expresada en grados decimales). Este dato fue elaborado a partir de la unión de todas las escenas individuales clasificadas que componen el Estado del AMZ en un mapa temático único",
        "date": "octubre 2014, con datos acumulados desde el año 1997",
        "description": ""
      },
      {
        "title": "Focos de calor",
        "source": "Instituto Nacional de Pesquisas Espaciales (INPE), vinculado al Ministerio de Ciencia y Tecnología (MCT)",
        "scale": "",
        "date": "actualización diaria, siendo siempre visibles los focos registrados en la víspera",
        "description": "<p>Un foco indica la existencia de fuego en un elemento de resolución de la imagen (píxel), que varía de 1 km x 1 km hasta 5 km x 4 km. En este píxel puede haber uno o varios incendios distintos, aunque la indicación sea de un solo foco. Y si el incendio es extenso, se verá en algunos píxeles vecinos, es decir que varios focos estarán asociados a una única y gran quemada. Satélites y canales: se utilizan todos los satélites que disponen de sensores ópticos que operan en el rango térmico – media de 4um – cuya señal el INPE puede recibir.</p><p>En la actualidad (julio del 2007), son procesadas operacionalmente AVHRR de los satélites polares NOAA-15, NOAA-16, NOAA-17, NOAA-18 y NOAA-19, las imágenes MODIS de los satélites polares NASA TERRA y AQUA, así como las imágenes de los satélites geoestacionarios GOES-12 y el MSG-2.  Cada satélite en órbita polar produce al menos un conjunto de imágenes al día, y los geoestacionarios generan algunas imágenes por hora, de modo que el INPE procesa en total más de 100 imágenes por día específicamente para detectar focos de quema de vegetación.<p>En el 2007 se espera iniciar la recepción de las imágenes AVHRR del nuevo satélite MetOp. Las recepciones se realizan en las estaciones de Cachoeira Paulista, en São Paulo, y Cuiabá, en Mato Grosso.</p>"
      },
      {
        "title": "Títulos mineros",
        "source": "Departamento Nacional de Producción Mineral (DNPM), vinculado al Ministerio de Minas y Energía",
        "scale": "",
        "date": "04/01/2016",
        "description": "Clases de agrupamiento:</strong>Debido a la gran cantidad de títulos, éstos fueron agrupados por etapa del proceso bajo una leyenda con 4 clases: <ol><li>1. Interesse em pesquisar: Requerimento de pesquisa</li> <li>2. Em pesquisa ou disponibilidade: Autorização de pesquisa; Disponibilidade</li> <li>3. Solicitação de extração: Requerimento de lavra; Requerimento de lavra garimpeira; Requerimento de licenciamento; Requerimento de registro de extração</li> <li>4. Autorización para extracción: Concessão de lavra; Lavra garimpeira; Licenciamento; Registro de extração</li></ol>"
      },
      {
        "title": "Energía",
        "source": "Agencia Nacional de Energía Eléctrica (ANEEL), vinculada al Ministerio de Minas y Energía (MME)",
        "scale": "sin incidencia",
        "date": "02/02/2016",
        "description": "Clases de agrupación:</strong> se agrupan por tipo (PCH - Pequeña Central Hidroeléctrica, UHE – Usina Hidroeléctrica, UTE - Termo-eléctrica) y por fase (en funcionamiento, construcción, concesión y planeamiento)"
      },
      {
        "title": "Cavernas",
        "source": "Base de datos del Centro Nacional de Investigación y Conservación de Cavernas (CECAV) del Instituto Chico Mendes de Conservación de la Biodiversidad (ICMBio), vinculado al Ministerio de Medio Ambiente (MMA)",
        "scale": "",
        "date": "31/12/2015",
        "description": "<p>En el 2004, el CECAV creó su base de datos de la ubicación de las cavernas de Brasil, que cuenta con el aporte permanente de informaciones provenientes de otras bases de datos, estudios espeleológicos, material bibliográfico y, en especial, trabajos de campo realizados por sus técnicos y analistas ambientales. Los datos no representan todo el universo de cuevas existentes en Brasil. Ellos reúnen sólo la pequeña parte de cavidades que ya han sido exploradas por particulares, grupos o instituciones, cuyos resultados fueron publicados en diversos medios de comunicación, y que fueron sistematizados, georreferenciados y analizados por el CECAV.<p></p>Desde el 2005, el CECAV ofrece en su página web parte de esta base de datos que incluye los datos validados por su equipo técnico o que muestran niveles mínimos de fiabilidad, que provienen de fuentes fiables, son citados por más de una fuente o cuya geoespacialidad se corresponde con las descripciones que los acompañan.</p><p>En la actualidad, la base del CECAV cuenta con alrededor de 17.000 registros, referentes a más de 7.000 cavernas; sabemos, sin embargo, que estos datos no reflejan el universo de cuevas conocidas en Brasil. En este sentido, el CECAV está llevando a cabo un inventario de las cavidades naturales subterráneas en Brasil.</p><p>Cavidades geoespacializadas a partir de la integración de datos procedentes de: encuestas de campo realizadas por el personal técnico del CECAV; estudios e investigaciones presentadas al CECAV/ICMBio; bibliografía especializada; CNC - Registro Nacional de Cavernas de Brasil; la Sociedad Brasileña de Espeleología - SBE; CODEX - Registro Nacional de Cavernas, REDESPELEO BRASIL. (Fuente: CECAV/ICMBio, septiembre de 2010)</p>"
      },
      /*
        {
          "title": "Noticias",
          "source": "Diversas, especificadas en cada caso",
          "scale": "",
          "date": "actualización diaria",
          "description": "Una de las actividades diarias del Programa de Monitoreo de Áreas Protegidas consiste en indexar noticias relacionadas, a partir de la búsqueda activa en fuentes primarias federales y estaduales, principalmente de los estados de la Amazonía Legal. Son monitoreadas cerca de 50 fuentes semanalmente, y de ellas 30 diariamente. Además de noticias directamente relacionadas con unidades de protección específicas, disponibles en este sitio, se indexan también las que abordan temas conexos, como Tierras y Pueblos Indígenas, políticas socioambientales, política forestal, presiones y amenazas en áreas protegidas e iniciativas locales, entre otras. En la actualidad, la base de datos en la cual ellas son indexadas abarca aproximadamente 80 mil noticias."
        },
        */
      {
        "title": "Sítios Ramsar",
        "source": "Serviço de Informações dos Sítios Ramsar (RSIS)",
        "scale": "",
        "date": "maio de 2010",
        "description": ""
      }
    ]
  },
  'pt-br': {
    "items": [
      {
        "title": "Terras Indígenas",
        "source": "Instituto Socioambiental (ISA), Programa de Monitoramento de Áreas Protegidas e Laboratório de Geoprocessamento",
        "scale": "1:100.000 na Amazônia Legal e 1:250.000 fora da Amazônia Legal",
        "date": "atualização permanente",
        "description": "<p>Acompanhamento dos atos de reconhecimento, criação, revogação e alteração de limites nos Diários Oficiais da União. A plotagem dos memoriais descritivos é realizada sobre uma base cartográfica na escala de 1:100.000 para a Amazônia e 1:250.000 para o restante do país. São utilizadas as seguintes bases cartográficas: compilação da base vetorial hidrográfica e estadual do DSG, IBGE e MMA na escala 1:100.000 para a Amazônia Legal e base vetorial contínua na escala 1:250:000 para o resto do Brasil (BC250 - IBGE, 2015).</p>"
      },
      {
        "title": "Unidades de Conservação (UCs), Mosaicos e Corredores",
        "source": "Instituto Socioambiental (ISA), Programa de Monitoramento de Áreas Protegidas",
        "scale": "1:100.000 (estaduais e federais na Amazônia Legal); 1:250.000 (federais fora da Amazônia Legal) e múltiplas escalas (estaduais fora da Amazônia Legal).",
        "date": "atualização permanente para UCs estaduais e federais na Amazônia legal, e São Paulo; e atualização periódica para os demais estados.",
        "description": "<p>Acompanhamento dos atos de reconhecimento, criação, revogação e alteração de limites nos Diários Oficiais da União e estados da Amazônia Legal. A plotagem dos memoriais descritivos é realizada sobre uma base cartográficana escala 1:100.000 para a Amazônia e 1:250.000 (ou melhor) para o restante do país. São utilizadas as seguintes bases cartográficas: compilação da base hidrográfica e estadual do DSG, IBGE e MMA na escala 1:100.000 para os estados da Amazônia Legal e base vetorial contínua na escala 1:250:000 para o resto do Brasil (BC250 - IBGE, 2015). No caso das UCs estaduais fora da Amazônia Legal, a base cartográfica é consolidada a partir de múltiplas fontes, através de busca direta junto aos órgãos gestores, Cadastro Nacional de UCs do MMA e outros órgãos oficiais.</p>"
      },
      {
        "title": "Biomas e Fitofisionomias",
        "source": "Instituto Brasileiro de Geografia e Estatística (IBGE), vinculado ao Ministério de Planejamento, Orçamento e Gestão",
        "scale": "1:5.000.000",
        "date": "2004 (primeira aproximação) / Setembro 2010",
        "description": "Classes de agrupamentos: Devido à grande quantidade de tipos de contatos entre as fitofisionomias, todos foram agrupados em uma classe única denominada 'contatos', ao serem visualizados nos mapas de página web. Disponível em: https://geoftp.ibge.gov.br/informacoes_ambientais/estudos_ambientais/biomas/vetores/"
      },
      {
        "title": "Bacias Hidrográfica Otto Pfaster",
        "source": "Agência Nacional de Águas (ANA)",
        "scale": "compatível com a escala 1:1.000.000. Classes de agrupamentos: níveis 1, 2 e 3 a depender da escala de visualização no mapa.",
        "date": "2012",
        "description": "Disponível em: https://metadados.snirh.gov.br/geonetwork/srv/api/records/1a2dfd02-67fd-40e4-be29-7bd865b5b9c5"
      },
      {
        "title": "Desflorestamento",
        "source": " Instituto Nacional de Pesquisas Espaciais (INPE). Coordenação Geral de Observação da Terra. Programa de monitoramento da Amazônia e Demais Biomas. Desmatamento consolidado para a Amazônia Legal (PRODES)",
        "scale": "Dado temático raster com resolução de 30 metros. Para mais informações sobre a metodologia, acesse: http://www.obt.inpe.br/OBT/assuntos/programas/amazonia/prodes/pdfs/Metodologia_Prodes_Deter_revisada.pdf",
        "date": "atualização anual, dado refere-se ao período de 01/ago/2000 até 31/jul/2020, última atualização em jun/2021outubro 2014, com dados acumulados desde o ano 1997",
        "description": "Disponível em: http://terrabrasilis.dpi.inpe.br/downloads/"
      },
      {
        "title": "Focos de calor",
        "source": "Instituto Nacional de Investigações Espaciais (INPE), vinculado ao Ministério de Ciência e Tecnologia e Inovação (MCTI). Um foco indica a existência de fogo em um elemento de resolução da imagem (pixel), que varia de 1 km x 1 km até 5 km x 5 km. Neste pixel pode haver um ou vários incêndios distintos, ainda que a indicação seja de um só foco. Utilizamos o satélite de referência AQUA_M-T (sensor MODIS, passagem no início da tarde). Para maiores detalhes acesse: http://www.inpe.br/queimadas/portal/informacoes/perguntas-frequentes",
        "scale": "",
        "date": "atualização diária, sendo sempre visíveis os focos registrados no dia anterior.",
        "description": "Disponível em: https://queimadas.dgi.inpe.br/queimadas/bdqueimadas#exportar-dados"
      },
      {
        "title": "Processos minerários",
        "source": "Agência Nacional de Mineração (ANM), Ministério de Minas e Energia",
        "scale": "",
        "date": "atualização semestral, dados baixados em 20/01/2022",
        "description": "os processos foram agrupados por etapa, sob uma legenda de 4 classes: interesse em pesquisar, pesquisa ou disponibilidade, solicitação de extração, autorização para extração. Disponível em: https://app.anm.gov.br/dadosabertos/SIGMINE/PROCESSOS_MINERARIOS/"
      },
      {
        "title": "Energia",
        "source": "Agência Nacional de Energia Elétrica (ANEEL), vinculada ao Ministério de Minas e Energia (MME)",
        "scale": "",
        "date": "atualização anual, dados baixados em 12/07/2021",
        "description": "os dados estão classificados em: PCH - Pequena Central Hidroelétrica, UHE – Usina Hidroelétrica e UTE - Termoelétrica. Usinas extintas ou canceladas não estão disponíveis para visualização no mapa. Disponível em: https://sigel.aneel.gov.br/Down/"
      },
      {
        "title": "Petróleo e Gás",
        "source": "Agência Nacional do Petróleo, Gás Natural e Biocombustíveis (ANP)",
        "scale": "",
        "date": "atualização anual, dados baixados no dia 12/07/2021",
        "description": "Visualização dos dados: campos de produção e blocos de exploração. Disponível em: http://geo.anp.gov.br/mapview"
      },
      {
        "title": "Caverna",
        "source": "Cadastro Nacional de Informações Espeleológicas (CANIE) - Base de dados do Centro Nacional de Investigação e Conservação de Cavernas (CECAV) do Instituto Chico Mendes de Conservação da Biodiversidade (ICMBio), vinculado ao Ministério do Meio Ambiente (MMA).",
        "scale": "",
        "date": "atualização anual, dados baixados em 08/12/2021",
        "description": "Disponível em: https://www.icmbio.gov.br/cecav/canie.html"
      },
      /*
        {
          "title": "Notícias",
          "source": "Diversas, especificadas em cada caso",
          "scale": "",
          "date": "atualização diária",
          "description": "Uma das atividades diárias do Programa de Monitoramento de Áreas Protegidas consiste em indexar notícias relacionadas, a partir da busca ativa em fontes primárias federais e estaduais, principalmente dos estados da Amazônia Legal.  São monitoradas cerca de 50 fontes semanalmente e aproximadamente 30 diariamente. Além de notícias diretamente relacionadas com unidades de conservação específicas disponíveis neste site, são indexadas também as que abordam temas correlatos, como Terras e Povos Indígenas, políticas socioambientais, política florestal, pressões e ameaças em áreas protegidas e iniciativas locais, entre outras. Atualmente, a base de dados de notícias abarca mais de 100 mil notícias."
        },
        */
      {
        "title": "Sítios Ramsar e Reservas da Biosfera",
        "source": "Ministério do Meio Ambiente, com adaptações",
        "scale": "",
        "date": "maio de 2018",
        "description": ""
      },
      {
        "title": "Limite da Amazônia Legal",
        "source": "Sistema de Proteção da Amazônia (SIPAM)/Instituto Brasileiro de Geografia e Estatística (IBGE)",
        "scale": "250.000",
        "date": "2004",
        "description": "limite conforme lei nº 1.806 de 06/01/1953"
      },

      {
        "title": "Limite da Mata Atlântica",
        "source": "Instituto Brasileiro de Geografia e Estatística (IBGE)",
        "scale": "",
        "date": "",
        "description": "limite do bioma da mata atlântica conforme lei nº 11.428 de 2006"
      }
    ]
  }
};
