import Vue                   from 'vue';
import { buildProps        } from './props.js';
import { mapComponentMixin } from './mixin.js';

// Setup component
var vm = {
  mixins: [ mapComponentMixin ],
  props : {},
}

// Add props
vm = buildProps(vm)

// Export
export default vm
