import * as mapHandler from '../instance.js';
import { varType }     from '../../common/types.js';

var globals = require('../globals');

export var mapComponentMixin = {
  props: {
    height: {
      type   : String,
      default: '400px',
    },
    width : {
      type   : String,
      default: '400px',
    },
  },

  data: function() {
    return {
      isamap     : null,
      params     : {},
      initialized: false,
    }
  },

  mounted: function() {
    this.setParams();
    this.initMap();
  },

  methods: {
    setParams: function() {
      // Cast params passed to the component
      // Make a copy instead of referencing the instance properties
      // See https://github.com/karol-f/vue-custom-element/issues/124
      //this.params = this._props;
      this.params = jQuery.extend(true, {}, this._props);

      // Some properties might need conversion
      for (var prop in this.params) {
        if (varType(globals.defaultSettings[prop]) == Array && varType(this.params[prop]) != Array) {
          this.params[prop] = JSON.parse(this.params[prop]);
        }
      }

      // Some need to be converted manually
      if (this.params.arps != null) {
        this.params.arps = JSON.parse(this.params.arps);
      }
    },
    initMap: function() {
      var self = this;

      // Register loaded event before asking for data load
      jQuery(document).on('isaMapLoaded', function() {
        if (!self.initialized) {
          // Initialize map
          self.isamap = mapHandler.add(self.$el, self.params);

          // Notify any upper Vue.js components
          self.$emit('input', self.isamap)
        }

        self.initialized = true;
      });

      // Load common data
      mapHandler.load('https://mapa.eco.br');
    },
  },

  computed: {
    getStyle: function() {
      return 'height: ' + this.height + '; width: ' + this.width + '; margin: 0; padding: 0; overflow: hidden;';
    },
  },

  watch: {
    lang() {
      this.isamap.methods.localeTransition(this.lang);
    },
  },
}
