import { varType } from '../../common/types.js';

var globals = require('../globals');

export function buildProps(vm) {
  // Register properties with their type and default values
  for (var prop in globals.defaultSettings) {
    // Handle null values as strings by default
    if (globals.defaultSettings[prop] == null) {
      vm.props[prop] = {
        default: globals.defaultSettings[prop],
        type   : String,
      };
    }
    // Arrays and objects need to set their default values as functions
    // https://vuejs.org/v2/guide/components-props.html
    else if (Array.isArray(globals.defaultSettings[prop])) {
      vm.props[prop] = {
        // A closure here preserves the current element
        default: (function() {
          var item = prop;

          return function() {
            return globals.defaultSettings[item];
          }
        })(),
        type: Array,
      };
    }
    else if (typeof(globals.defaultSettings[prop]) == 'object') {
      vm.props[prop] = {
        // A closure here preserves the current element
        default: (function() {
          var item = prop;

          return function() {
            return globals.defaultSettings[item];
          }
        })(),
        type: Object,
      };
    }
    else {
      vm.props[prop] = {
        default: globals.defaultSettings[prop],
        type   : varType(globals.defaultSettings[prop]),
      };
    }
  }

  return vm
}
