//import L from '../leaflet.js';

export function layers(self) {
  return {
    gisLayerUrl: function(layer) {
      if (self.geo.services.gisLayers[layer] != undefined) {
        return self.geo.services.gisLayers[layer].url + '/' + self.geo.services.gisLayers[layer].layer;
      }
    },

    showLayer: function(group, layer) {
      if (self.layers[group] == undefined || self.layers[group].layers[layer] == undefined) {
        return;
      }

      self.methods.debug('Showing Layer ' + group + '.' + layer + '...');

      // Pan out
      //
      // This also captures all loaded features as L.esri.featureLayer load event
      // fires only for features added in the current bounds.
      //
      // This is not always the case, so why we keep it optional.
      if (self.layers[group].layers[layer].panOut == true || self.layers[group].layers[layer].fitBounds == true) {
        self.map.setZoom(self.minZoom);
        self.map.panTo(L.latLng(self.mapCenter));
      }

      // Tell the user we're loading
      //self.methods.showOverlay(group + '.' + layer);
      self.methods.statusMessageLoading({ name: group + '.' + layer });

      self.layers[group].layers[layer].layer.once('load', function(e) {
        // Tell the user we're ready
        //self.methods.unblockUI(group + '.' + layer);
        self.methods.statusMessageReady(group + '.' + layer);

        if (self.layers[group].layers[layer].fitBounds != undefined && self.layers[group].layers[layer].fitBounds == true) {
          self.methods.debug('Fit bounds for ' + group + '.' + layer + '...');

          //self.map.flyToBounds(self.layers[group].layers[layer].bounds);
          self.map.fitBounds(self.layers[group].layers[layer].bounds);
        }
      });

      //if (self.methods.hasLayer(group, layer)) {
      //  if (self.layers[group].layers[layer].layer.options.opacity != undefined) {
      //    var opacity = self.layers[group].layers[layer].layer.options.opacity;
      //  }
      //  else {
      //    var opacity = 1;
      //  }

      //  self.layers[group].layers[layer].layer.setOpacity(opacity);
      //}
      //else {
      //  self.layers[group].layers[layer].layer.addTo(self.map);
      //}
      self.layers[group].layers[layer].layer.addTo(self.map);

      // Update self.settings.layers since addTo does not trigger and overlayadd event
      self.methods.addLayerIntoSettings(group, layer);
    },

    hideLayer: function(group, layer) {
      self.methods.debug('Removing Layer ' + group + '.' + layer +'...');

      //self.layers[group].layers[layer].layer.setOpacity(0);
      self.map.removeLayer(self.layers[group].layers[layer].layer);

      // Update self.settings.layers since hideLayer does not trigger and overlayremove event
      self.methods.hideLayerFromSettings(group, layer);
    },

    hasLayer: function(group, layer) {
      if (self.layers[group] == undefined) {
        return false;
      }

      if (self.layers[group].layers[layer] == undefined) {
        return false;
      }

      if (self.map.hasLayer(self.layers[group].layers[layer].layer)) {
        return true;
      }

      return false;
    },

    hasBaseLayer: function(group, layer) {
      if (self.geo.layers.base[group] == undefined) {
        return false;
      }

      if (self.geo.layers.base[group].layers[layer] == undefined) {
        return false;
      }

      if (self.map.hasLayer(self.geo.layers.base[group].layers[layer].layer)) {
        return true;
      }

      return false;
    },

    toggleLayer: function(group, layer) {
      self.methods.debug('Toggling Layer ' + group + '.' + layer + '...');

      if (self.methods.hasLayer(group, layer)) {
        self.methods.hideLayer(group, layer);
      }
      else {
        self.methods.showLayer(group, layer);
      }
    },

    activateBaseLayer: function(group, layer) {
      self.methods.debug('Activating BaseLayer ' + group + '.' + layer + '...');

      // Remove all other base layers
      for (var section in self.geo.layers.base) {
        for (var item in self.geo.layers.base[section].layers) {
          if (section == group && item == layer) {
            continue;
          }

          self.map.removeLayer(self.geo.layers.base[section].layers[item].layer);
        }
      }

      self.geo.layers.base[group].layers[layer].layer.addTo(self.map);
    },
  }
}
