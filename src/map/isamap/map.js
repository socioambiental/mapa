//import L from '../leaflet.js';

export function map(self) {
  return {
    myMap: function() {
      return self.map;
    },

    createMap: function() {
      self.methods.debug('Creating map #' + self.settings.mapId + '...');

      // Create the map object
      self.map = L.map(self.settings.mapId, {
        center            : self.mapCenter,
        zoom              : self.mapZoom,
        maxZoom           : self.maxZoom,
        maxBounds         : self.maxBounds,
        zoomControl       : false,
        fullscreenControl : {
          pseudoFullscreen: false,
        },
      });

      // Set baselayer
      if (self.settings.baseLayer != null) {
        var split     = self.settings.baseLayer.split('.');
        var group     = split[0];
        split.splice(0, 1);
        var baselayer = split.join('.');

        self.methods.debug('Setting ' + self.settings.baseLayer + ' as base layer...');

        if (self.geo.layers.base[group]['layers'][baselayer].layer != undefined) {
          self.map.addLayer(self.geo.layers.base[group]['layers'][baselayer].layer);
        }
        else {
          self.geo.layers.base.base.layers['topographic'].layer.addTo(self.map);
        }
      }

      // Layer preprocessing
      for (var group in self.layers) {
        // Set slider IDs: now this is done directly in the slider control
        //if (self.layers[group].slider) {
        //  self.layers[group].sliderId = group;

        //  // Set slider initial from value
        //  for (var layer in self.layers[group].layers) {
        //    if (self.settings.layers.indexOf(group + '.' + layer) != -1) {
        //      self.layers[group].slider.from = layer;
        //    }
        //  }
        //}

        // Register events
        for (var layer in self.layers[group].layers) {
          if (self.layers[group].layers[layer].layer != undefined) {

            if (self.layers[group].layers[layer].click != undefined) {
              self.methods.debug('Registering click event for Layer ' + group + '.' + layer + '...');
              self.layers[group].layers[layer].layer.on('click', self.layers[group].layers[layer].click);
            }

            if (self.layers[group].layers[layer].dblclick != undefined) {
              self.methods.debug('Registering dblclick event for Layer ' + group + '.' + layer + '...');
              self.layers[group].layers[layer].layer.on('dblclick', self.layers[group].layers[layer].dblclick);
            }

            if (self.layers[group].layers[layer].mousedown != undefined) {
              self.methods.debug('Registering mousedown event for Layer ' + group + '.' + layer + '...');
              self.layers[group].layers[layer].layer.on('mousedown', self.layers[group].layers[layer].mousedown);
            }

            if (self.layers[group].layers[layer].mouseover != undefined) {
              self.methods.debug('Registering mouseover event for Layer ' + group + '.' + layer + '...');
              self.layers[group].layers[layer].layer.on('mouseover', self.layers[group].layers[layer].mouseover);
            }

            if (self.layers[group].layers[layer].mouseout != undefined) {
              self.methods.debug('Registering mouseout event for Layer ' + group + '.' + layer + '...');
              self.layers[group].layers[layer].layer.on('mouseout', self.layers[group].layers[layer].mouseout);
            }

            if (self.layers[group].layers[layer].popupopen != undefined) {
              self.methods.debug('Registering popupopen event for Layer ' + group + '.' + layer + '...');
              self.layers[group].layers[layer].layer.on('popupopen', self.layers[group].layers[layer].popupopen);
            }

            if (self.layers[group].layers[layer].popupclose != undefined) {
              self.methods.debug('Registering popupclose event for Layer ' + group + '.' + layer + '...');
              self.layers[group].layers[layer].layer.on('popupclose', self.layers[group].layers[layer].popupclose);
            }

            if (self.layers[group].layers[layer].contextmenu != undefined) {
              self.methods.debug('Registering contextmenu event for Layer ' + group + '.' + layer + '...');
              self.layers[group].layers[layer].layer.on('contextmenu', self.layers[group].layers[layer].contextmenu);
            }

            if (self.layers[group].layers[layer].bindPopup != undefined) {
              self.methods.debug('Binding a popup for Layer ' + group + '.' + layer + '...');
              self.layers[group].layers[layer].layer.bindPopup(self.layers[group].layers[layer].bindPopup);
            }

            if (self.layers[group].layers[layer].add != undefined) {
              self.methods.debug('Registering add event for Layer ' + group + '.' + layer + '...');
              self.layers[group].layers[layer].layer.on('add', self.layers[group].layers[layer].add);
            }

            if (self.layers[group].layers[layer].remove != undefined) {
              self.methods.debug('Registering remove event for Layer ' + group + '.' + layer + '...');
              self.layers[group].layers[layer].layer.on('remove', self.layers[group].layers[layer].remove);
            }

            // Block UI while loading for layers implementing the 'loading' event
            self.layers[group].layers[layer].layer.on('loading', function() {
              //self.methods.showOverlay(group + '.' + layer);
              self.methods.statusMessageLoading({ name: group + '.' + layer });
            });

            // Unblock UI when loaded for layers implementing the 'load' event
            self.layers[group].layers[layer].layer.on('load', function() {
              //self.methods.unblockUI(group + '.' + layer);
              self.methods.statusMessageReady(group + '.' + layer);
            });
          }
        }
      }

      // Add map controls
      self.methods.layerControl();

      // Pointer position
      self.methods.mouseControl();

      // Adds scale ruller
      self.methods.scaleControl();

      // Zoom control
      self.methods.zoomControl();

      // Watermark control
      self.methods.watermarkControl();

      // Locale control
      self.methods.localeControl();

      // Legend control
      self.methods.legendControl();

      // Status control
      self.methods.statusControl();

      // Technical note control
      self.methods.technicalNote();

      // Embed widget
      self.methods.embedCodeControl();

      // Print control
      self.methods.printControl();

      // FileLayer control
      self.methods.fileLayerControl();

      // Register custom controls
      if (self.settings.customInitMethods.length > 0) {
        self.settings.customInitMethods.forEach(function(custom) {
          custom.call(this, self);
        });
      }

      // Apply scrolling configuration
      if (self.settings.scrollWheelZoom == false) {
        self.map.scrollWheelZoom.disable();
      }

      // Identify features from all layers
      // See https://esri.github.io/esri-leaflet/examples/identifying-features.html
      self.map.on('click', function (e) {
        var content     = '';
        var hashContent = {};

        for (var group in self.layers) {
          for (var layer in self.layers[group].layers) {
            if (self.layers[group].layers[layer].layer != undefined) {
              if (self.layers[group].layers[layer].identify != undefined) {
                if (!self.map.hasLayer(self.layers[group].layers[layer].layer)) {
                  continue;
                }

                self.methods.debug('Identifying Layer ' + group + '/' + layer + '...');

                // A closure here preserves the current element
                (function(element) {
                  // Force identification only of the layer in question
                  // See https://developers.arcgis.com/rest/services-reference/identify-map-service-.htm
                  //     https://esri.github.io/esri-leaflet/api-reference/tasks/identify-features.html#options
                  var layers = 'visible:' + element.layer.options.layers.join(',');

                  element.layer.identify().on(self.map).at(e.latlng).layers(layers).run(function(error, featureCollection) {
                    var result = element.identify(error, featureCollection);

                    if (result != false) {
                      var hash = btoa(result);

                      // Workaround to avoid duplicated content if layers identify the same thing
                      if (hashContent[hash] == undefined) {
                        content           += result;
                        hashContent[hash]  = true;
                      }
                    }

                    if (content != undefined && content != 'undefined') {
                      self.methods.popup(e.latlng, content);
                    }
                  });
                })(self.layers[group].layers[layer]);
              }
            }
          }
        }
      });

      // Register events for state tracking
      self.methods.mapStateTracking();
    },
  }
}
