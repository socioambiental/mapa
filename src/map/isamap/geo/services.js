export function services(self) {
  var gisServices = {
    'tematicos'     : self.settings.gisServer + 'tematicos',
    'desmatamento'  : self.settings.gisServer + 'tematicos/desmatamento/MapServer',
    'monitoramento' : self.settings.gisServer + 'monitoramento',
    'arps'          : self.settings.gisServer + 'monitoramento/arps/MapServer',
    'tis'           : self.settings.gisServer + 'monitoramento/tis/MapServer',
    'ucs'           : self.settings.gisServer + 'monitoramento/ucs/MapServer',
    'ucs_v1'        : self.settings.gisServer + 'monitoramento/ucs_v1/MapServer',
  };

  return {
    // Define main GIS Server
    gisServer : self.settings.gisServer,

    // Define main GIS Services
    gisServices: gisServices,

    // Define main GIS Layers
    gisLayers : {
      'arpsMarkers'                   : { url : gisServices.arps,                                    layer : 0  },
      'arpsLimits'                    : { url : gisServices.arps,                                    layer : 1  },
      'tisMarkers'                    : { url : gisServices.tis,                                     layer : 2  },
      'tisLimits'                     : { url : gisServices.tis,                                     layer : 0  },
      'ucsMarkersEstaduais'           : { url : gisServices.ucs_v1,                                  layer : 1  },
      'ucsMarkersFederais'            : { url : gisServices.ucs_v1,                                  layer : 4  },
      'ucsLimitsEstaduais'            : { url : gisServices.ucs_v1,                                  layer : 2  },
      'ucsLimitsFederais'             : { url : gisServices.ucs_v1,                                  layer : 5  },
      //'ucsMosaicosCorredores'       : { url : gisServices.ucs,                                     layer : 6  },
      'rbiosf'                        : { url : gisServices.ucs_v1,                                  layer : 9  },
      'mosaics'                       : { url : gisServices.ucs_v1,                                  layer : 7  },
      'corridors'                     : { url : gisServices.ucs_v1,                                  layer : 8  },
      'ramsar'                        : { url : gisServices.ucs_v1,                                  layer : 10 },
      'energia'                       : { url : gisServices.tematicos + '/energia/MapServer',        layer : 0  },
      'mineracao'                     : { url : gisServices.tematicos + '/mineracao/MapServer',      layer : 0  },
      'petroleo'                      : { url : gisServices.tematicos + '/petroleo/MapServer',       layer : 0  },
      'amlegal'                       : { url : gisServices.tematicos + '/limites_v1/MapServer',     layer : 0  },
      'mata_atlantica'                : { url : gisServices.tematicos + '/limites_v1/MapServer',     layer : 1  },
      'biomas'                        : { url : gisServices.tematicos + '/vegetacao/MapServer',      layer : 1  },
      'cavernas'                      : { url : gisServices.tematicos + '/cavernas/MapServer',       layer : 0  },
      'fitofisionomia'                : { url : gisServices.tematicos + '/vegetacao/MapServer',      layer : 2  },
      'bacias'                        : { url : gisServices.tematicos + '/bacias/MapServer',         layer : 1  },
      'ottobacias'                    : { url : gisServices.tematicos + '/bacias/MapServer',         layer : 2  },
      'focos'                         : { url : gisServices.tematicos + '/focos/MapServer',          layer : 0  },
    },
  }
}
