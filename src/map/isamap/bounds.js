//import L from '../leaflet.js';

var globals = require('../globals');

export function bounds(self) {
  return {
    restoreDefaultBounds: function() {
      self.methods.debug('Restoring initial center and zoom...');
      self.map.setZoom(self.initialSettings.zoom);
      self.map.panTo(L.latLng(globals.defaultSettings.center));
      self.map.setView(L.latLng(globals.defaultSettings.center), self.initialSettings.zoom);
    },
  }
}
