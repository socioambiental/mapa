var globals = require('../globals');

export function init(self) {
  return {
    // Initialize a map
    initialize: function() {
      // Set layer definition
      if (self.settings.layerDefinitions != null) {
        if (self.settings.layerDefinitionsExtend == true) {
          self.layers = jQuery.extend(true, {}, self.settings.layerDefinitions, self.geo.layers.default);
        }
        else {
          self.layers = self.settings.layerDefinitions;
        }
      }
      else {
        self.layers = self.geo.layers.default;
      }

      // Exclude some groups
      if (self.settings.groupExcluded.length > 0) {
        for (var excluded in self.settings.groupExcluded) {
          if (self.layers[self.settings.groupExcluded[excluded]] != undefined ) {
            delete self.layers[self.settings.groupExcluded[excluded]];
          }
        }
      }

      // Exclude some layers
      if (self.settings.layerExcluded.length > 0) {
        for (var excluded in self.settings.layerExcluded) {
          var split     = self.settings.layerExcluded[excluded].split('.');
          var subgroup  = split[0];
          split.splice(0, 1);
          var layer     = split.join('.');

          if (self.layers[subgroup] != undefined && self.layers[subgroup].layers[layer] != undefined) {
            delete self.layers[subgroup].layers[layer];
          }
        }
      }

      // Exclude some base layers
      if (self.settings.baseLayerExcluded.length > 0) {
        for (var excluded in self.settings.baseLayerExcluded) {
          var split     = self.settings.baseLayerExcluded[excluded].split('.');
          var subgroup  = split[0];
          split.splice(0, 1);
          var layer     = split.join('.');

          if (self.geo.layers.base[subgroup] != undefined && self.geo.layers.base[subgroup].layers[layer] != undefined) {
            delete self.geo.layers.base[subgroup].layers[layer];
          }
        }
      }

      // Set control configuration
      if (self.settings.expandedGroups.length != 0) {
        for (var g = 0; g < self.settings.expandedGroups.length; g++) {
          if (self.layers[self.settings.expandedGroups[g]].expanded != undefined) {
            self.layers[self.settings.expandedGroups[g]].expanded = true;
          }
        }
      }

      // Ensure we have an 'arps' group
      if (self.layers.arps == undefined) {
        self.layers.arps = {
          groupName   : 'ARPs',
          expanded    : false,
          layerControl: false,
          layers      : { },
        };
      }

      // Create the map
      self.methods.createMap();

      // Search control is initialized here as it depends on metadata
      self.methods.searchControl();

      // Display ARPs
      if (self.settings.arps != null && self.settings.arps != false && self.settings.arps.length != 0) {
        // Due to performance concerns, do not plot limits if there are too many selected ARPs
        if (self.settings.arps.length <= globals.maxArpsWithLimits) {
          self.methods.addArpsById({ arps: self.settings.arps, type: 'markers', show: true, fitBounds: true });
          self.methods.addArpsById({ arps: self.settings.arps, type: 'limits',  show: true });
        }
        else {
          self.methods.addArpsById({ arps: self.settings.arps, type: 'markers', show: true });
        }
      }
      else {
        // Convert to array
        self.settings.arps = [];
      }

      // Add ARP group layers (ti.uf.AM etc)
      for (var group in globals.arpsByProperty) {
        for (var subgroup in globals.arpsByProperty[group]) {
          for (var layer in globals.arpsByProperty[group][subgroup]) {
            self.methods.addArpsByGroup({
              group : group,
              name  : subgroup + '.' + layer,
              show  : false,
            });
          }
        }
      }

      // Display custom layer selection
      for (var l = 0; l < self.settings.layers.length; l++) {
        var split     = self.settings.layers[l].split('.');
        var subgroup  = split[0];
        split.splice(0, 1);
        var layer     = split.join('.');

        if (subgroup != undefined && layer != undefined) {
          self.methods.showLayer(subgroup, layer);
        }
      }

      // Trigger initialization event
      jQuery(self.settings.overlaySelector).trigger('isaMapReady');
    },
  }
}
