//import L from '../leaflet.js';

var globals = require('../globals');

export function arps(self) {
  return {
    hasArp: function(id, type) {
      layerId = type + '_' + id;

      if (self.layers.arps.layers[layerId] != undefined) {
        return true;
      }

      return false;
    },

    tiIcon: function(data) {
      //if (data != undefined && data.situacao_juridica != undefined) {
      //  if (data.situacao_juridica == 'RESERVADA. REG CRI E SPU.' || data.situacao_juridica == 'HOMOLOGADA.') {
      //    return self.settings.imagesUrl + '/ti/ti_homologada.png';
      //  }
      //  else if (data.situacao_juridica == 'DECLARADA.') {
      //    return self.settings.imagesUrl + '/ti/ti_declarada.png';
      //  }
      //  else if (data.situacao_juridica == 'IDENTIFICADA.') {
      //    return self.settings.imagesUrl + '/ti/ti_identificada.png';
      //  }
      //  else if (data.situacao_juridica == 'EM IDENTIFICAÇÃO.') {
      //    return self.settings.imagesUrl + '/ti/ti_identificacao.png';
      //  }
      //  else if (data.situacao_juridica == 'RESERVADA.') {
      //    return self.settings.imagesUrl + '/ti/ti_reservada.png';
      //  }
      //  else if (data.situacao_juridica == 'COM RESTRIÇÃO DE USO') {
      //    return self.settings.imagesUrl + '/ti/ti_restricao.png';
      //  }
      //  else {
      //    return self.settings.imagesUrl + '/ti/ti_square.png';
      //  }
      //}
      if (data != undefined && data.grupo_etapa != undefined) {
        if (data.grupo_etapa == 'Registrada no CRI e/ou SPU' || data.grupo_etapa == 'Homologada') {
          return self.settings.imagesUrl + '/ti/ti_homologada.png';
        }
        else if (data.grupo_etapa == 'Declarada') {
          return self.settings.imagesUrl + '/ti/ti_declarada.png';
        }
        else if (data.grupo_etapa == 'Identificada') {
          return self.settings.imagesUrl + '/ti/ti_identificada.png';
        }
        else if (data.grupo_etapa == 'Em Identificação') {
          return self.settings.imagesUrl + '/ti/ti_identificacao.png';
        }
        else if (data.grupo_etapa == 'Reservada') {
          return self.settings.imagesUrl + '/ti/ti_reservada.png';
        }
        else if (data.grupo_etapa == 'Com restrição de uso a não índios') {
          return self.settings.imagesUrl + '/ti/ti_restricao.png';
        }
        else {
          return self.settings.imagesUrl + '/ti/ti_square.png';
        }
      }
      else {
        return self.settings.imagesUrl + '/ti/ti_square.png';
      }
    },

    buildArpWhere: function(params) {
      var id       = null;
      params.where = '';

      if (params.whereCondition == undefined) {
        params.whereCondition = ' OR ';
      }

      // By sorting the array we can make sure we always get the same hash
      params.arps.sort(self.sortNumber);

      // Build query conditions
      for (var i = 0; i < params.arps.length; i++) {
        id            = params.arps[i];
        params.where += 'id_arp=' + id + params.whereCondition;
      }

      // Remove trailing 'OR' from query builder
      params.where = params.where.substring(0, params.where.length - 3);

      return params.where;
    },

    addArpsByGroup: function(params) {
      // Check if group is defined
      if (self.layers[params.group] == undefined) {
        self.layers[params.group] = { 'layers': {}};
      }

      // Check if it's already defined
      if (self.layers[params.group].layers[params.name] != undefined) {
        if (params.show != undefined && params.show == true) {
          self.methods.showLayer(params.group, params.name);
        }

        return;
      }

      // Determine ARP IDs
      var split    = params.name.split('.');
      var subgroup = split[0];
      split.splice(0, 1);
      var layer    = split.join('.');
      params.arps  = globals.arpsByProperty[params.group][subgroup][layer];

      // Build where condition
      params.where = self.methods.buildArpWhere(params);

      // Build layer
      self.methods.addArpLayer(params);
    },

    addArpsById: function(params) {
      // Set group
      params.group = 'arps';

      // Set type and base name
      if (params.type == undefined) {
        params.type = 'markers';
        params.name = 'markers';
      }
      else {
        params.name = params.type;
      }

      if (params.arps == undefined || params.arps.length == 0) {
        return;
      }

      for (var i = 0; i < params.arps.length; i++) {
        var id = params.arps[i];

        if (!jQuery.isNumeric(id)) {
          continue;
        }

        // Build unique layer name
        params.name += '_' + id;

        // Update self.settings.arps for state tracking
        if (self.settings.arps.indexOf(id) == -1) {
          self.settings.arps.push(id);
        }
      }

      // Check if it's already defined
      if (self.layers[params.group].layers[params.name] != undefined) {
        if (params.show != undefined && params.show == true) {
          self.methods.showLayer(params.group, params.name);
        }

        return;
      }

      // Build where condition
      params.where = self.methods.buildArpWhere(params);

      // Build layer
      self.methods.addArpLayer(params);
    },

    addArpLayer: function(params) {
      // Check if group is defined
      if (self.layers[params.group] == undefined) {
        self.layers[params.group] = { 'layers': {}};
      }

      // Check if it's already defined
      if (self.layers[params.group].layers[params.name] != undefined) {
        if (params.show != undefined && params.show == true) {
          self.methods.showLayer(params.group, params.name);
        }

        return;
      }

      // Set type and base name
      if (params.type == undefined) {
        params.type = 'markers';
      }

      //self.methods.debug('Registering Layer ' + params.group + '.' + params.name + '...');

      var layerName = 'arps' + params.type.substring(0, 1).toUpperCase() + params.type.substring(1).toLowerCase();
      var service   = self.methods.gisLayerUrl(layerName);

      // Define layer
      if (params.type == 'markers') {
        self.layers[params.group].layers[params.name] = {
          description: params.group + '.' + params.name,
          layer: L.esri.featureLayer({
            url          : service,
            where        : params.where,
            opacity      : 1,
            proxy        : self.settings.proxyUrl,
            cacheLayers  : true,
            //token        : apiKey,
            pointToLayer : function (geojson, latlng) {
              var id = geojson.properties.id_arp;

              if (geojson.properties.tipo_aps == 'TI') {
                //var icon     = self.settings.imagesUrl + '/ti/ti_feather.png';
                //var iconSize = [22, 28];

                var icon     = self.methods.tiIcon(globals.arps.tis[geojson.properties.id_arp]);
                var iconSize = [32, 32];
              }
              else if (geojson.properties.tipo_aps == 'UCE') {
                var icon     = self.settings.imagesUrl + '/uc/estadual.png';
                var iconSize = [16, 24];
              }
              else if (geojson.properties.tipo_aps == 'UCF') {
                var icon     = self.settings.imagesUrl + '/uc/federal.png';
                var iconSize = [16, 24];
              }

              if (params.fitBounds != undefined && params.fitBounds == true) {
                self.layers[params.group].layers[params.name].fitBounds = true;

                // Here we use area_isa instead of official area because it's more
                // compatible with the plots and hence it's a better reference
                // to calculate boundaries
                if (globals.arps.tis[id] != undefined) {
                  var area = globals.arps.tis[id].area_isa;
                }
                else if (globals.arps.ucs[id] != undefined) {
                  var area = globals.arps.ucs[id].area_isa;
                }

                // We used this as an area estimate/average when we did not had
                // area information
                //var toBounds = 100000;

                // From hectare get an average distance in meters
                var toBounds = Math.sqrt(area) * 10^4;

                // Then get a bigger area to make sure the ARP will fit the screen
                toBounds = toBounds * 25;

                self.layers[params.group].layers[params.name].bounds.extend(L.latLng(latlng.lat, latlng.lng).toBounds(toBounds));
              }

              var tooltip = self.methods.arpPopupContents({ properties: { id_arp: id } }, false);

              return L.marker(latlng, {
                icon: L.icon({
                  iconUrl:  icon,
                  iconSize: iconSize,
                }),
              }).bindTooltip(tooltip);
            },
          }),
          click: function(e) {
            var content = self.methods.arpPopupContents(e.layer.feature);

            e.layer.bindPopup(content);
            e.layer.openPopup();
          },
          // Create a new empty Leaflet bounds object
          bounds: L.latLngBounds([]),
        };
      }
      else {
        self.layers[params.group].layers[params.name] = {
          description: params.group + '.' + params.name,
          layer: L.esri.featureLayer({
            url     : service,
            proxy   : self.settings.proxyUrl,
            //token   : apiKey,
            where   : params.where,
            opacity : 0.4,
            style: function(feature) {
              if (feature.properties.tipo_arps === 'TI') {
                return {color: '#FF0000', weight: 3 };
              }
              else if (feature.properties.tipo_arps === 'UCF') {
                return {color: '#A59237', weight: 3 };
              }
              else if (feature.properties.tipo_arps === 'UCE') {
                return {color: '#4B832D', weight: 3 };
              }
            },
          }),
        };
      }

      // Ensure we pan out
      self.layers[params.group].layers[params.name].panOut = true;

      // Register load event
      if (params.load != undefined) {
        self.layers[params.group].layers[params.name].layer.on('load', params.load);
      }

      // Register mouseover event
      if (params.mouseover != undefined) {
        self.layers[params.group].layers[params.name].layer.on('mouseover', params.mouseover);
      }

      // Register mouseout event
      if (params.mouseout != undefined) {
        self.layers[params.group].layers[params.name].layer.on('mouseout', params.mouseout);
      }

      // Register popupopen event
      if (params.popupopen != undefined) {
        self.layers[params.group].layers[params.name].layer.on('popupopen', params.popupopen);
      }

      // Register popupclose event
      if (params.popupclose != undefined) {
        self.layers[params.group].layers[params.name].layer.on('popupclose', params.popupclose);
      }

      // Register click event
      if (self.layers[params.group].layers[params.name].click != undefined) {
        self.layers[params.group].layers[params.name].layer.on('click', self.layers[params.group].layers[params.name].click);
      }
      else if (params.click != undefined) {
        self.layers[params.group].layers[params.name].layer.on('click', params.click);
      }

      if (params.show != undefined && params.show == true) {
        // Add to map: on sucess, self.layers.arps.layers[layerId].layer._layers._leaflet_id is set
        //self.layers[params.group].layers[params.name].layer.addTo(self.map);
        self.methods.showLayer(params.group, params.name);
      }
    },

    removeArpsById: function(arps, type) {
      if (self.layers.arps == undefined) {
        return;
      }

      if (type == undefined) {
        type = 'markers';
      }

      // If no argument is passed, operate in all available ARPs.
      if (arps == undefined || arps == null) {
        self.methods.debug('Removing all ARP layers...');

        for (name in self.layers.arps.layers) {
          self.methods.hideLayer('arps', name);
        }

        self.methods.removeBasicArpLayers();

        // Update state tracking
        self.settings.arps = [];
      }
      else {
        var name = null;

        // By sorting the array we can make sure we always get the same hash
        arps.sort(self.sortNumber);

        for (var i = 0; i < arps.length; i++) {
          var id = arps[i];
          name   = type + '_' + id;
          index  = self.settings.arps.indexOf(id);

          // Update self.settings.arps for state tracking
          if (index != -1) {
            self.settings.arps.splice(index, 1);
          }

          if (self.layers.arps.layers[name] != undefined) {
            self.methods.hideLayer('arps', name);
          }
        }

        // Already done by hideLayer
        //updateEncodedSettings();
      }
    },

    removeBasicArpLayers: function() {
      self.methods.hideLayer('tis', 'limits');
      self.methods.hideLayer('tis', 'markers');
      self.methods.hideLayer('ucs', 'markersEstaduais');
      self.methods.hideLayer('ucs', 'markersFederais');
      self.methods.hideLayer('ucs', 'limitsEstaduais');
      self.methods.hideLayer('ucs', 'limitsFederais');
    },

    removeArpsByGroup: function(group) {
      self.methods.removeBasicArpLayers();

      if (group == undefined || group == null) {
        var groups = [ 'tis', 'ucs', 'arps' ];

        // Handle special case
        self.settings.arps = [];
        self.methods.updateEncodedSettings();
      }
      else {
        var groups = [ group ];
      }

      for (var group in groups) {
        if (self.layers[groups[group]] != undefined) {
          for (var layer in self.layers[groups[group]].layers) {
            // Hide only marker layers that are currently shown
            //if (self.layers[groups[group]].layers[layer].layer.options.pointToLayer != undefined && self.methods.hasLayer(groups[group], layer)) {
            if (self.methods.hasLayer(groups[group], layer)) {
              self.methods.hideLayer(groups[group], layer);
            }
          }
        }
      }
    },
  }
}
