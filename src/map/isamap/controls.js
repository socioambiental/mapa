import Vue     from 'vue';
import VueI18n from 'vue-i18n';
Vue.use(VueI18n);

import watermarkComponent     from '../controls/watermark/component.vue';
import localeComponent        from '../controls/locale/component.vue';
import technicalNoteComponent from '../controls/technical_note/component.vue';
import embedCodeComponent     from '../controls/embed_code/component.vue';
import searchComponent        from '../controls/search/component.vue';
import legendComponent        from '../controls/legend/component.vue';
import layerComponent         from '../controls/layer/component.vue';
import printComponent         from '../controls/print/component.vue';
//import L                    from '../leaflet.js';

export function controls(self) {
  return {
    registerControl: function(name, tid, aid) {
      // Create a control
      // See https://leafletjs.com/examples/extending/extending-3-controls.html
      L.Control[name] = L.Control.extend({
        onAdd: function(map) {
          // Load the HTML from a template
          var template = jQuery('#' + tid);
          var element  = jQuery(template.html())[0];

          // Set element id
          jQuery(element).attr('id', aid);

          return element;
        },
      });

      // Control alias
      L.control[name.toLowerCase()] = function(opts) {
        return new L.Control[name](opts);
      }

      // Return the modified L object to ensure the caller get's the proper instance
      return L;
    },

    registerVueControl: function(name, aid, options, component, componentOptions = null) {
      // Append the map ID into the element id so the DOM is not monopolized
      aid += '-' + self.settings.mapId;

      // Create a control placeholder for a VueJS component
      // See https://leafletjs.com/examples/extending/extending-3-controls.html
      L.Control[name] = L.Control.extend({
        onAdd: function(map) {
          var element = L.DomUtil.create('div');
          element.id  = aid;

          return element;
        },
      });

      // Control alias
      L.control[name.toLowerCase()] = function(opts) {
        return new L.Control[name](opts);
      }

      if (options.attachTo != undefined && options.attachTo != null) {
        var control = null;
        var id      = options.attachTo;
      }
      else {
        // Add control into the map
        var id      = aid;
        var control = L.control[name.toLowerCase()]({ position: options.position }).addTo(self.map);
      }

      // Set i18n before instantiating the component
      component.i18n = self.i18n;

      // Instantiate a VueJS App
      // Thanks https://css-tricks.com/creating-vue-js-component-instances-programmatically/
      var ComponentClass  = Vue.extend(component);
      var instance        = new ComponentClass();

      // Set basic control params
      Vue.set(instance, 'isamap',  self);
      Vue.set(instance, 'locale',  self.settings.lang);
      Vue.set(instance, 'map',     self.map);
      Vue.set(instance, 'control', control);
      Vue.set(instance, 'id',      aid);

      // Set other component options
      if (componentOptions != null) {
        for (var option in componentOptions) {
          Vue.set(instance, option, componentOptions[option]);
        }
      }

      // Mount instance into the DOM
      instance.$mount('#' + id);

      // Register into the global isaMap state
      self.controls[name.toLowerCase()] = instance;

      // Return the modified L object to ensure the caller get's the proper instance
      return L;
    },

    // Alias
    watermarkControl: function() {
      var options = {
        logo      : self.settings.logo       != null ? self.settings.logo       : self.settings.imagesUrl + '/logos/microisa.png',
        width     : self.settings.logo_width != null ? self.settings.logo_width : '94px',
        href      : self.settings.logo_href  != null ? self.settings.logo_href  : 'https://www.socioambiental.org/',
        position  : 'bottomleft',
        aboutHref : 'https://mapa.socioambiental.org/pages/about',
      }

      var componentOptions = {
        options: options,
        style  : 'width: ' + options.width + 'px;',
      }

      // Register the control
      self.methods.registerVueControl('Watermark', 'leaflet-watermark-control', options, watermarkComponent, componentOptions);
    },

    // Alias
    localeControl: function() {
      if (self.settings.localeControl == true || self.settings.localeControl == 'true') {
        var options = {
          position: self.settings.localeControlPosition,
        }

        // Register the control
        self.methods.registerVueControl('Locale', 'leaflet-locale-control', options, localeComponent);
      }
    },

    technicalNote: function() {
      if (self.settings.techNoteControl == true || self.settings.techNoteControl == 'true') {
        var options = {
          position: 'topright',
        }

        // Register the control
        self.methods.registerVueControl('technical_note', 'leaflet-technical_note-control', options, technicalNoteComponent);
      }
    },

    statusControl: function() {
      L.Control.Status = L.Control.extend({
        onAdd: function(map) {
          var div  = L.DomUtil.create('div');
          var span = L.DomUtil.create('span', '', div);
          span.id  = self.settings.mapId + '_status';

          return div;
        },
      });

      L.control.status = function(opts) {
        return new L.Control.Status(opts);
      }

      self.controls.status = L.control.status({ position: self.settings.statusControlPosition }).addTo(self.map);
    },

    // Alias
    legendControl: function() {
      if (self.settings.legendControl != false && self.settings.legendControl != 'false') {
        var options = {
          position: 'topright',
        }

        var componentOptions = {
          options  : {
            localize         : self.settings.localizeContent,
            collapseSimple   : true,
            detectStretched  : true,
            collapsedOnInit  : true,
            collapsed        : true,
            position         : 'topright',
            visibleIcon      : 'icon icon-eye',
            hiddenIcon       : 'icon icon-eye-slash',
            //defaultOpacity : 0.5,
          }
        }

        // Register the control
        self.methods.registerVueControl('Legend', 'leaflet-legend-control', options, legendComponent, componentOptions);
      }
    },

    embedCodeControl: function() {
      if (self.settings.embedControl != false && self.settings.embedControl != 'false') {
        var options = {
          position: 'topright',
        }

        // Register the control
        self.methods.registerVueControl('embed_code', 'leaflet-embed_code-control', options, embedCodeComponent);
      }
    },

    searchControl: function() {
      if (self.settings.searchControl != false && self.settings.searchControl != 'false') {
        var options = {
          position: 'topright',
        }

        // Register the control
        self.methods.registerVueControl('Search', 'leaflet-search-control', options, searchComponent);
      }
    },

    zoomControl: function() {
      self.controls.zoom = L.control.zoom({
        position: 'bottomright',
      }).addTo(self.map);
    },

    scaleControl: function() {
      self.controls.scale = L.control.scale({
        position:       'bottomright',
        metric:         true,
        imperial:       false,
        updateWhenIdle: true
      }).addTo(self.map);
    },

    mouseControl: function() {
      self.controls.mouse = L.control.mousePosition({
        position:    'bottomright',
        emptyString: 'Sem posição...',
        prefix:      'Coordenadas: ',
        separator:   ','
      }).addTo(self.map);
    },

    layerControl: function() {
      var options = {
        position        : self.settings.layerControlPosition,
        collapsed       : true,
        container_width : '250px',
        group_maxHeight : '300px',
        debug           : self.settings.debug,
        localize        : self.settings.localizeContent,
      }

      if (self.settings.layerControlAttachTo != null) {
        options.attachTo = self.settings.layerControlAttachTo;
      }

      var componentOptions = {
        state   : self.settings.layerControlState,
        base    : jQuery.extend(true, {}, self.geo.layers.base),
        layers  : jQuery.extend(true, {}, self.layers), // Extend, so runtime layers aren't included
        options : options,
      }

      // Register the control
      self.methods.registerVueControl('Layer', 'leaflet-layer-control', options, layerComponent, componentOptions);
    },

    // Alias
    printControl: function() {
      if (self.settings.printControl != false && self.settings.printControl != 'false') {
        var options = {
          position: 'bottomleft',
        }

        // Register the control
        self.methods.registerVueControl('Print', 'leaflet-print-control', options, printComponent);
      }
    },

    fileLayerControl: function() {
      if (self.settings.fileLayerControl != false && self.settings.fileLayerControl != 'false') {
        var style = {
          color       : 'red',
          opacity     : 1.0,
          fillOpacity : 1.0,
          weight      : 2,
          clickable   : true
        };

        L.Control.FileLayerLoad.LABEL = '<img class="icon" src="/images/misc/folder.svg" alt="file icon"/>';
        L.Control.fileLayerLoad({
          //fileSizeLimit : 1024,
          fitBounds     : true,
          position      : 'bottomright',
          layer         : L.geoJson,
          addToMap      : true,
          layerOptions  : {
            style: style,
            pointToLayer: function (data, latlng) {
              return L.circleMarker(
                latlng,
                { style: style }
              );
            }
          },
        }).addTo(self.map);
      }
    },
  }
}
