// Global variables
// We use module.exports instead "export var globals" for Node.js compatibility
module.exports = {
  majorVersion      : '2',

  // API
  apiBasicAuth      : 'mapa:WQxilp83J/9AQA==',
  apiKey            : 'Qqd04sojeXEpivTvLtUhndDvRC8wZ6D7',
  apiDataLoadStatus : { 'ucs': false, 'tis': false, 'ufs': false, 'peoples': false, },
  ufs               : {},

  // jQuery plugin
  instances         : {},

  // Geocoding
  geocode           : {},

  // ARPs
  maxArpsWithLimits : 100,
  peoples           : {},
  arps              : { 'ucs': {}, 'tis': {} },
  arpsByProperty    : { 'ucs': {}, 'tis': {} },
  arpsProperties    : {
    'tis': [
      'categoria',
      'jurisdicao_legal',
      'funai',
      'sesai',
      //'situacao_juridica',
      'grupo_etapa',
      'uf',
      'presenca_isolados',
      'populacao',
      'area',
      'faixa_fronteira',
      'total_povos_residentes',
      'povo',
      'bacia',
      'bioma',
      'fitofisionomia',
      'pressao',
    ],
    'ucs': [
      'categoria',
      'instancia_responsavel',
      'jurisdicao_legal',
      'bacia',
      'bioma',
      'fitofisionomia',
      'pressao',
    ],
  },

  // blockUI stylesheet
  blockUIStyle : {
    'font-size'       : '18px',
    'border'          : 'none',
    'backgroundColor' : 'rgb(255, 255, 255) transparent',
  },

  // Default settings
  defaultSettings : {
    lang                   : 'pt-br',
    about                  : true,
    arps                   : null,
    map                    : null,
    proxyUrl               : null,
    center                 : [ -15, -55 ],
    zoom                   : 5,
    minZoom                : 4,
    maxZoom                : 15,
    maxBounds              : [ [-35, -98], [15, -14] ],
    expandedGroups         : [ ],
    groupExcluded          : [ ],
    layers                 : [ 'tis.limits', 'ucs.limitsEstaduais', 'ucs.limitsFederais', 'jurisdicao.amlegal' ],
    layerExcluded          : [ ],
    layerDefinitions       : null,
    layerDefinitionsExtend : false,
    baseLayerExcluded      : [ 'base.imagery', 'base.terrain', 'base.community', 'base.streets', 'base.humanitary', ],
    baseLayer              : 'base.topographic',

    // We can use either the API Gateway or the direct Geo URL; for the first case we have to ensure
    // the apiKey is sent using the '//token' param at each layer definition; the API Gateway endpoint
    // must accept '//token' as a valid auth param.
    //
    // Right now using the API Gateway is not working as L.esri still access some URLs not using the
    // token parameter, such as /query endpoints. A more robust sollution would involve a public
    // Geo Proxy hosted somewhere like https://mapa.eco.br/proxy or fix L.esri.
    //
    // If you want to use the direct Geo Service, you'll have to keep commented all '//token' params from
    // layer definitions.
    //gisServer            : 'https://api.socioambiental.org/geo/v1/webadaptor1/rest/services/',
    gisServer              : 'https://geo.socioambiental.org/webadaptor1/rest/services/',

    baseUrl                : 'https://mapa.eco.br/',        //'https://mapa.socioambiental.org/',
    imagesUrl              : 'https://mapa.eco.br/images/', //'https://mapa.socioambiental.org/images/', // dev: 'https://gitlab.com/socioambiental/jquery.isamap/raw/feature/leaflet/images/'
    pagesUrl               : 'https://mapa.socioambiental.org/pages/',
    geocodeUrl             : 'https://maps.googleapis.com/maps/api/geocode/json?address=',
    layerControlState      : 'closed',
    layerControlPosition   : 'topleft',
    layerControlAttachTo   : null,
    searchControl          : true,
    embedControl           : true,
    techNoteControl        : true,
    localeControl          : true,
    fileLayerControl       : true,
    localeControlPosition  : 'bottomleft',
    legendControl          : true,
    statusControlPosition  : 'bottomleft',
    printControl           : true,
    customInitMethods      : [ ],
    additionalLegend       : [ ],
    customMessages         : null,
    localizeContent        : true, // whether to localize only the interface or also layer and legend content
    urlChange              : true,
    scrollWheelZoom        : true,
    debug                  : false,
    logo                   : null,
    logo_width             : null,
    logo_href              : null,
    overlayCss             : {
      'font-size': '18px',
    },
  }
}
