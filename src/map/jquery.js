import { init   } from './init.js';
import { plugin } from './jquery/plugin.js';

(function ($) {
  // Get base URL
  var baseUrl = document.currentScript.getAttribute('data-base-url');

  // Register the plugin into jQuery registry
  jQuery.fn.isaMap = plugin;

  // Run global procedures
  jQuery(document).ready(init(baseUrl));
}(jQuery));
