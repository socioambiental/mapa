import 'leaflet';
import 'leaflet.markercluster';
import 'leaflet-mouse-position';
import 'leaflet.heat';
import 'leaflet-fullscreen';
import 'leaflet-extra-markers';
import 'leaflet-layerjson';
import 'leaflet-draw';
import fileLayer         from 'leaflet-filelayer';
import * as esriGeocoder from 'esri-leaflet-geocoder';
import * as esri         from 'esri-leaflet';
import * as esriCluster  from 'esri-leaflet-cluster';
import * as esriHeat     from 'esri-leaflet-heatmap';

import 'leaflet/dist/leaflet.css';
import 'leaflet-mouse-position/src/L.Control.MousePosition.css';
import 'leaflet.markercluster/dist/MarkerCluster.css';
import 'leaflet.markercluster/dist/MarkerCluster.Default.css';
import 'leaflet-fullscreen/dist/leaflet.fullscreen.css';
import 'esri-leaflet-geocoder/dist/esri-leaflet-geocoder.css';
import 'leaflet-draw/dist/leaflet.draw.css';
import 'leaflet-extra-markers/dist/css/leaflet.extra-markers.min.css';

// Include leaflet-esri objects into L
L.esri           = esri;
L.esri.Cluster   = esriCluster;
L.esri.Heat      = esriHeat;
L.esri.Geocoding = esriGeocoder;

// Initialize the fileLayer control
// See https://github.com/makinacorpus/Leaflet.FileLayer/issues/60
fileLayer();

export default L;
