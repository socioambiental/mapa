import { messages }  from '../messages.js';
import * as instance from '../instance.js';

var globals = require('../globals');

/**
 * jQuery plugin wrapper.
 */
export function plugin(options) {
  if (options == undefined) {
    var options = {};
  }
  else if (jQuery.isEmptyObject(options)) {
    options = {};
  }

  // Set current instance
  var current = instance.setId(this);

  // Initialize and dispatch
  if (globals.instances[current] == null) {
    instance.add(this, options);
  }
  else {
    // See http://www.iainjmitchell.com/blog/exposing-jquery-functions/
    //     http://stackoverflow.com/questions/12880256/jquery-plugin-creation-and-public-facing-methods
    if (typeof arguments[0] === 'string') {
      var args = Array.prototype.slice.call(arguments);
      args.splice(0, 1);

      if (globals.instances[current].methods[arguments[0]] != undefined) {
        return globals.instances[current].methods[arguments[0]].apply(this, args);
      }
      else {
        console.error('[jquery.isamap] [#' + current + '] Invalid method ' + arguments[0]);
      }
    }
  }
}
