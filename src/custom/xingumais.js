import Vue from 'vue';

import { getQueryVariable  } from '../common/url_query.js';
import { updateQueryString } from '../common/url_query.js';
import xingumais_map         from './xingumais/components/map/component.vue';
import map_mobile_menu       from './xingumais/components/mobile-menu/component.vue';
import template              from './xingumais.thtml';

import '../css/map.css';

// Wrapper
var element = jQuery(template)[0];
var vm      = new Vue({
  components: {
    'xingumais-map': xingumais_map,
    'map-mobile-menu': map_mobile_menu
  },
  data: {
    map       : '',
    defaultMap: 'degradacao',
  },
  mounted: function() {
    var map = this.getQueryVariable('map');

    // Load custom map
    if (map != null && map.length > 0 && this.map != map) {
      this.map = map;
    }
    else {
      this.map = this.defaultMap;
    }

    var url   = this.updateQueryString('map', this.map);
    var title = document.title;

    window.history.replaceState({}, title, url);
  },
  methods: {
    getQueryVariable: getQueryVariable,
    updateQueryString: updateQueryString,
  }
});

// Append template and mount the app
document.body.appendChild(element);
vm.$mount(element);
