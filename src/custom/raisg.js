import Vue from 'vue';

import raisg_map from './raisg/map/component.vue';
import template  from './raisg.thtml';

import '../css/map.css';

// Wrapper
var element = jQuery(template)[0];
var vm      = new Vue({
  components: {
    'raisg-map': raisg_map,
  },
});

// Append template and mount the app
document.body.appendChild(element);
vm.$mount(element);
