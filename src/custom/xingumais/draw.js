/**
 * pt-BR translations for Leaflet.draw.
 *
 * @module d8ox-leaflet-draw_pt-BR
 *
 * @see {@link https://github.com/Leaflet/Leaflet.draw|Leaflet.draw}
 *
 * @version 1.0
 * @author <a href='https://mexapi.macpress.com.br/about'>@denydias</a>
 * @license GPL-2.0+
 * @copyright ©2016 Instituto Socioambiental
 */

export var drawLocal = {
  draw: {
    toolbar: {
      actions: {
        title : 'Cancelar desenho.',
        text  : 'Cancelar'
      },
      finish: {
        title : 'Terminar desenho.',
        text  : 'Terminar'
      },
      undo: {
        title : 'Apagar último ponto desenhado.',
        text  : 'Apagar Último Ponto'
      },
      buttons: {
        polyline  : 'Desenhar uma linha',
        polygon   : 'Desenhar um polígono',
        rectangle : 'Desenhar um retângulo',
        circle    : 'Desenhar um círculo',
        marker    : 'Criar um marcador'
      }
    },
    handlers: {
      circle: {
        tooltip: {
          start: 'Clique e arraste para desenhar um círculo.'
        },
        radius: 'Raio'
      },
      circlemarker: {
        tooltip: {
          start: 'Clique no mapa para desenhar um marcador circular.'
        }
      },
      marker: {
        tooltip: {
          start: 'Clique no mapa para posicionar a denúncia.'
        }
      },
      polygon: {
        tooltip: {
          start : 'Clique para para começar a desenhar um polígono.',
          cont  : 'Clique para continuar desenhando.',
          end   : 'Clique no primeiro ponto para terminar.'
        }
      },
      polyline: {
        error: '<strong>Erro:</strong> arestas não podem se cruzar!',
        tooltip: {
          start : 'Clique para começar a desenhar uma linha.',
          cont  : 'Clique para continuar desenhando.',
          end   : 'Clique duas vezes para terminar.'
        }
      },
      rectangle: {
        tooltip: {
          start: 'Clique e arraste para desenhar a área do alerta.'
        }
      },
      simpleshape: {
        tooltip: {
          end: 'Solte para finalizar o desenho.'
        }
      }
    }
  },
  edit: {
    toolbar: {
      actions: {
        save: {
          title : 'Salvar mudanças.',
          text  : 'Salvar'
        },
        cancel: {
          title : 'Cancelar edição e descartar mudanças.',
          text  : 'Cancelar'
        }
      },
      buttons: {
        edit           : 'Editar desenho',
        editDisabled   : 'Não há desenhos para editar.',
        remove         : 'Apagar desenho',
        removeDisabled : 'Não há desenhos para apagar.'
      }
    },
    handlers: {
      edit: {
        tooltip: {
          text    : 'Arraste os quadrados ou marcadores para editar.',
          subtext : 'Clique em Cancelar para desfazer as mudanças.'
        }
      },
      remove: {
        tooltip: {
          text: 'Clique numa desenho para apagar.'
        }
      }
    }
  }
};
