//import L from '../../map/leaflet.js';

/**
 * Build geocoder providers.
 */
export function providers(geoBase) {
  var indigenousProvider = L.esri.Geocoding.featureLayerProvider({
    url              : geoBase + '/xingu_tis/MapServer/2',
    searchFields     : ['nome_ti'],
    label            : 'Terras Indígenas',
    bufferRadius     : 300,
    caseInsensitive  : true,
    formatSuggestion : function(feature){
      return feature.properties.nome_ti;
    },
  });

  /**
   * Geocode search provider: federal protected areas.
   *
   * @function
   *
   * @return {void}
   *
   * @requires d8ox-esri-leaflet-geocoder
   * @see {@link https://github.com/Esri/esri-leaflet-geocoder|Esri Leaflet Geocoder}
   */
  var fedProvider = L.esri.Geocoding.featureLayerProvider({
    url              : geoBase + '/xingu_ucs/MapServer/2',
    searchFields     : ['nome_uc'],
    label            : 'UCs Federais',
    bufferRadius     : 300,
    caseInsensitive  : true,
    formatSuggestion : function(feature){
      return feature.properties.nome_uc;
    },
  });

  /**
   * Geocode search provider: state protected areas.
   *
   * @function
   *
   * @return {void}
   *
   * @requires d8ox-esri-leaflet-geocoder
   * @see {@link https://github.com/Esri/esri-leaflet-geocoder|Esri Leaflet Geocoder}
   */
  var stateProvider = new L.esri.Geocoding.featureLayerProvider({
    url              : geoBase + '/xingu_ucs/MapServer/4',
    searchFields     : ['nome_uc'],
    label            : 'UCs Estaduais',
    bufferRadius     : 300,
    caseInsensitive  : true,
    formatSuggestion : function(feature){
      return feature.properties.nome_uc;
    },
  });

  /**
   * Geocode search provider: counties.
   *
   * @function
   *
   * @return {void}
   *
   * @requires d8ox-esri-leaflet-geocoder
   * @see {@link https://github.com/Esri/esri-leaflet-geocoder|Esri Leaflet Geocoder}
   */
  var countyProvider = L.esri.Geocoding.featureLayerProvider({
    url              : geoBase + '/xingu_limites/MapServer/4',
    searchFields     : ['nome'],
    label            : 'Municípios',
    bufferRadius     : 300,
    caseInsensitive  : true,
    formatSuggestion : function(feature){
      return feature.properties.nome + ', ' + feature.properties.UF;
    },
  });

  return [countyProvider, indigenousProvider, fedProvider, stateProvider];
}
