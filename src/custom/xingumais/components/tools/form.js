/**
 * This module declares the resources required by Xingu maps.
 *
 * @version 1.0.1
 * @author <a href='https://mexapi.macpress.com.br/about'>@denydias</a>
 * @license GPL-2.0+
 * @copyright ©2016 Instituto Socioambiental
 */

import 'tooltipster';
import { breakpoint } from './breakpoint.js';
//import L            from '../../../map/leaflet.js';

// Common variables
export var drawLayer;
export var drawAlert;
export var drawComplaint;
export var searchResult = {};

// Search button
export function getSearchBtn() {
  return jQuery('#tab-alert #alert-search-btn');
}

// Alert drawn button
export function getDrawRectBtn() {
  return jQuery('#tab-alert #alert-draw-btn');
}

// Complaint draw button
export function getDrawMarkBtn() {
  return jQuery('#tab-complaint #complaint-marker-btn');
}

// Reset complaint draw button if active
export function resetDrawMarkBtn(map) {
  var $drawMarkBtn = getDrawMarkBtn();

  if ($drawMarkBtn.hasClass('active')) {
    // Task is active
    drawComplaint.disable();
    $drawMarkBtn.removeClass('active');

    //console.debug('Ongoing complaint task has been canceled.');
  }
  else if ($drawMarkBtn.hasClass('selected')) {
    // Previously selected
    map.removeLayer(drawLayer);
    $drawMarkBtn.toggleClass('selected');

    // Clear any previous hidden form field values
    jQuery('#marker').val('');

    //console.debug('Existing values for complaint marker has been cleared.');
  }
}

// Reset alert search button if active or selected
export function resetSearchBtn(map) {
  var $searchBtn = getSearchBtn();

  if ($searchBtn.hasClass('active')) {
    // Task is active
    $searchBtn.removeClass('active');

    //console.debug('Ongoing alert search task has been canceled.');
  }
  else if ($searchBtn.hasClass('selected')) {
    // Previously selected
    map.removeLayer(drawLayer);
    $searchBtn.removeClass('selected');

    // Clear any previous hidden form field values
    searchResult = {};
    jQuery('#geo_name').val('');
    jQuery('#arp_id').val('');
    jQuery('#bb_ne').val('');
    jQuery('#bb_sw').val('');
    $searchBtn.prop('title', 'Pesquisar UCs, TIs ou municípios');

    //console.debug('Existing values for alert search has been cleared.');
  }
}

export function resetDrawRectBtn(map) {
  var $drawRectBtn = getDrawRectBtn();

  // Reset alert draw button if active or selected
  if ($drawRectBtn.hasClass('active')) {
    // Task is active
    drawAlert.disable();
    $drawRectBtn.removeClass('active');

    //console.debug('Ongoing alert draw task has been canceled.');
  }
  else if ($drawRectBtn.hasClass('selected')) {
    // Previously selected
    map.removeLayer(drawLayer);
    $drawRectBtn.removeClass('selected');

    // Clear any previous hidden form field values
    jQuery('#bb_ne').val('');
    jQuery('#bb_sw').val('');

    //console.debug('Existing values for alert draw has been cleared.');
  }
}

/**
 * Provides a draw control handler.
 *
 * @function drawHandler
 *
 * @param {string} type The shape type to draw: `rectangle` for alerts, `marker` for complaints.
 * @param {object} map  The map object.
 *
 * @returns {Object} The draw layer.
 *
 * @see {@link https://github.com/Leaflet/Leaflet.draw|Leaflet.draw}
 * @see {@link https://github.com/coryasilva/Leaflet.ExtraMarkers|Leaflet.extra-markers}
 */
export function drawHandler(type, map) {
  // Set default prefix for L.ExtraMarkers
  L.ExtraMarkers.Icon.prototype.options.prefix = 'fa';

  // Custom marker: complaint
  var complaintMarker = L.ExtraMarkers.icon({
    icon        : 'fa-exclamation-triangle',
    markerColor : 'red',
    shape       : 'penta'
  });

  // Handlers options
  var drawOptions = {
    draw: {
      rectangle: {
        shapeOptions: {
          color   : '#b81d00',
          weight  : 1,
          opacity : 0.85
        }
      },
      marker: {
        icon: complaintMarker,
      }
    }
  };

  // The handler itself
  if (type === 'rectangle') {
    var drawInit = new L.Draw.Rectangle(map, drawOptions.draw[type]);
  } else if (type === 'marker') {
    var drawInit = new L.Draw.Marker(map, drawOptions.draw[type]);
  }

  // Return the draw handler
  return drawInit;
}

/**
 * Add a draw control for toolboxes and handle its events, including form filling.
 * On mobile, also handles parent dialog deactivation to leave users with more real estate for drawing.
 *
 * @function drawControl
 *
 * @param {object} map The map object.
 *
 * @returns {void}
 *
 * @requires d8ox-degmap~initDegMap
 * @requires d8ox-degmap~drawHandler
 * @see {@link https://www.drupal.org/node/2269515|Drupal 8 Javascript API}
 */
export function addDrawTool(map) {
  // Init draw handler
  drawAlert = drawHandler('rectangle', map);

  var $drawRectBtn = getDrawRectBtn();

  // Draw button
  jQuery('#tab-alert #alert-draw-btn').click(function () {
    // Reset other selection tools
    resetSearchBtn(map);
    resetDrawMarkBtn(map);

    // Reset button state and values if previous results were set
    if ($drawRectBtn.hasClass('selected')) {
      // Previously selected
      map.removeLayer(drawLayer);
      $drawRectBtn.removeClass('selected');

      // Clear any previous hidden form field values
      jQuery('#bb_ne').val('');
      jQuery('#bb_sw').val('');

      //console.debug('Existing values for alert draw has been cleared.');
    }
    // Disable handler on second click
    else if ($drawRectBtn.hasClass('active')) {
      drawAlert.disable();
      $drawRectBtn.removeClass('active');
      // If on mobile, restore toolbar button states
      if (breakpoint() === 'default') {
        jQuery('.btn.map.alert').addClass('active');
        jQuery('.btn.map.alert').removeClass('inprogress');
        jQuery('.btn.map.layers, .btn.map.complaint, .btn.menu-btn').removeClass('inactive');
      }

      //console.debug('Alert draw was deactivaded.');
    }
    else {
      // Update UI elements: area metadata and button state
      jQuery('.action.area .area_name').html('aguardando seu desenho...');
      jQuery('.action.area .area_coord').html('aguardando seu desenho...');
      $drawRectBtn.addClass('active');

      // If on mobile, close the parent dialog, set toolbar button to 'in progress' and disable the other buttons
      if (breakpoint() === 'default') {
        jQuery('#tab-alert').dialog('close');
        jQuery('.btn.map.alert').toggleClass('active');
        jQuery('.btn.map.alert').addClass('inprogress');
        jQuery('.btn.map.layers, .btn.map.complaint, .btn.menu-btn').addClass('inactive');
      }
      //else {
      //  // Prevent tab change
      //  jQuery('#tabs-toolbox').tabs('option', 'disabled', [1]);
      //}

      // Enable alert draw handler
      drawAlert.enable();

      //console.debug('Drawing an alert rectangle...');
    }
  });

  // Draw events
  // When a shape (rectangle or marker) is created, store its properties into hidden form values and do some UX magic to keep the workflow
  map.on('draw:created', function (e) {
    var ltype = e.layerType;
    var layer = e.layer;

    // Handle alerts values (rectangles)
    if (ltype === 'rectangle') {
      // If rectangle, get its bound box and add a fine popup in case user click over it

      var latLngs = layer.getLatLngs();
      var area    = L.GeometryUtil.geodesicArea(latLngs[0]) / 100 / 100;
      var area_ha = area.toLocaleString('pt-BR', {maximumFractionDigits: 2});

      layer.bindPopup('<header>Área do Alerta: ' + area_ha + ' ha</header>');

      // Fill in bounding box
      jQuery('#bb_ne').val(latLngs[0][2].lat + ',' + latLngs[0][2].lng);
      jQuery('#bb_sw').val(latLngs[0][0].lat + ',' + latLngs[0][0].lng);

      // Update UI elements: area metadata
      jQuery('.action.area .area_name').html('personalizada, ' + area_ha + ' ha');
      jQuery('.action.area .area_coord').html(
        latLngs[0][2].lat.toLocaleString('en', { maximumFractionDigits: 2 }) + ','      +
        latLngs[0][2].lng.toLocaleString('en', { maximumFractionDigits: 2 }) + '(NE), ' +
        latLngs[0][0].lat.toLocaleString('en', { maximumFractionDigits: 2 }) + ','      +
        latLngs[0][0].lng.toLocaleString('en', { maximumFractionDigits: 2 }) + '(SW)');

      //console.debug('Alert rectangle has been drawn.');

      // Handle complaints values (markers)
    }
    else if (ltype === 'marker') {
      // If marker, get its position and add a fine popup in case user click over it
      var latLng = layer.getLatLng();
      var lat    = latLng.lat.toLocaleString('pt-BR', { maximumFractionDigits: 5 });
      var lng    = latLng.lng.toLocaleString('pt-BR', { maximumFractionDigits: 5 });

      layer.bindPopup('<header>Local da Denúncia: ' + lat + ', ' + lng + '</header>');

      // Fill in position
      jQuery('#marker').val(latLng.lat + ',' + latLng.lng);
    }

    // Add layer and its popup to map
    layer.addTo(map);

    // Persist the new layer for posterity
    drawLayer = layer;

    // Handle alerts UX
    if (ltype === 'rectangle') {
      // If on desktop, focus next field
      if (breakpoint() === 'enhanced') {
        jQuery('#alert_description_box').show();
        jQuery('#alert_description_box').focus();
        //jQuery('#tab-alert #firstname').focus();
      }

      // Add selected state, remove active state and call user attention back to parent form
      jQuery('#tab-alert #alert-draw-btn').addClass('selected');
      jQuery('#tab-alert #alert-draw-btn').removeClass('active');
      jQuery('#tab-alert').stop(true, true).effect('highlight', {color: '#d66b58'}, 300);

      // Handle complaint UX
    }
    else if (ltype === 'marker') {
      // If on desktop, focus next field
      if (breakpoint() === 'enhanced') {
        jQuery('#tab-complaint #title').focus();
      }

      // Add selected state, remove active state and call user attention back to parent form
      jQuery('#tab-complaint #complaint-marker-btn').addClass('selected');
      jQuery('#tab-complaint #complaint-marker-btn').removeClass('active');
      jQuery('#tab-complaint').stop(true, true).effect('highlight', {color: '#d66b58'}, 300);
    }

    // Reenable tabs
    //if (breakpoint() === 'enhanced') {
    //  jQuery('#tabs-toolbox').tabs('option', 'disabled', false);
    //}
  });

  // When shape drawing is done (or canceled), do other kinds of UX magic to keep the workflow
  map.on('draw:drawstop', function (e) {
    var ltype = e.layerType;

    // Handle alert UX
    if (ltype === 'rectangle') {
      // If on mobile, reopen the parent dialog and restore buttons states
      if (breakpoint() === 'default') {
        jQuery('#tab-alert').dialog('open');
        jQuery('.btn.map.alert').addClass('active');
        jQuery('.btn.map.alert').removeClass('inprogress');
        jQuery('.btn.map.layers, .btn.map.complaint, .btn.menu-btn').toggleClass('inactive');
      }

      // If there is no draw
      if (jQuery('#alert-draw-btn').hasClass('active')) {
        // Update UI elements: area metadata
        jQuery('.action.area .area_name').html('pesquise ou desenhe uma área...');
        jQuery('.action.area .area_coord').html('aguardando pesquisa ou desenho...');
      }

      // Remove button active state
      jQuery('#tab-alert #alert-draw-btn').removeClass('active');

      // Handle complaint UX
    }
    else if (ltype === 'marker') {
      // If on mobile, first reopen the parent form
      if (breakpoint() === 'default') {
        jQuery('#tab-complaint').dialog('open');
        jQuery('.btn.map.complaint').addClass('active');
        jQuery('.btn.map.complaint').toggleClass('inprogress');
        jQuery('.btn.map.layers, .btn.map.alert, .btn.menu-btn').toggleClass('inactive');
      }

      // Remove button active state
      jQuery('#tab-alert #complaint-draw-btn').removeClass('active');
    }

    // Reenable tabs
    //if (breakpoint() === 'enhanced') {
    //  jQuery('#tabs-toolbox').tabs('option', 'disabled', false);
    //}
  });
}

// Handle search results for alerts
export function addSearchTool(map, searchControl) {
  var $searchBtn = getSearchBtn();

  // Toolbox button controls
  // In alert dialog
  // Search button
  jQuery('#tab-alert #alert-search-btn').click(function () {
    // Reset other selection tools
    resetDrawRectBtn(map);
    resetDrawMarkBtn(map);

    jQuery('#alert_description_box').hide();

    // Reset button state and values if previous results were set
    if ($searchBtn.hasClass('selected')) {
      // Previously selected
      map.removeLayer(drawLayer);
      $searchBtn.removeClass('selected');

      // Clear any previous hidden form field values
      searchResult = {};
      jQuery('#geo_name').val('');
      jQuery('#arp_id').val('');
      jQuery('#bb_ne').val('');
      jQuery('#bb_sw').val('');
      $searchBtn.prop('title', 'Pesquisar UCs, TIs ou municípios');

      //console.debug('Existing values for alert search has been cleared.');
    }
    // Disable handler on second click
    else if ($searchBtn.hasClass('active')) {
      $searchBtn.removeClass('active');

      // Update UI elements: area metadata
      jQuery('.action.area .area_name').html('pesquise ou desenhe uma área...');
      jQuery('.action.area .area_coord').html('aguardando pesquisa ou desenho...');

      // If on mobile, reopen the parent dialog and restore buttons states
      if (breakpoint() === 'default') {
        jQuery('#tab-alert').dialog('open');
        jQuery('.btn.map.alert').addClass('active');
        jQuery('.btn.map.alert').removeClass('inprogress');
        jQuery('.btn.map.layers, .btn.map.complaint, .btn.menu-btn').toggleClass('inactive');
      }
      //else {
      //  jQuery('#tabs-toolbox').tabs('option', 'disabled', false);
      //}

      //console.debug('Alert by search was deactivaded.');
    }
    else {
      // Update UI elements: area metadata and button state
      jQuery('.action.area .area_name').html('aguardando sua pesquisa...');
      jQuery('.action.area .area_coord').html('aguardando sua pesquisa...');
      $searchBtn.addClass('active');

      // If on mobile, close the parent dialog, set toolbar button to 'in progress' and disable the other buttons
      if (breakpoint() === 'default') {
        jQuery('#tab-alert').dialog('close');
        jQuery('.btn.map.alert').toggleClass('active');
        jQuery('.btn.map.alert').addClass('inprogress');
        jQuery('.btn.map.layers, .btn.map.complaint, .btn.menu-btn').addClass('inactive');
      }
      //else {
      //  // Prevent tab change
      //  jQuery('#tabs-toolbox').tabs('option', 'disabled', [1]);
      //}

      // Focus and highlight the search box to call user attention
      //jQuery('input.geocoder-control-input').focus().effect('highlight', {color: '#d66b58'}, 300);

      //jQuery('#mapToolsSearch').trigger('click');
      //jQuery('input.geocoder-control-input').tooltipster('content', 'Pesquise a região para acompanhamento.');

      //console.debug('Searching for an alert location...');
    }
  });

  searchControl.on("results", function(data) {
    // Define the search alert button to interact with
    var $searchBtn = getSearchBtn();

    // Reenable tabs on desktop
    //if (breakpoint() === 'enhanced') {
    //  jQuery('#tabs-toolbox').tabs('option', 'disabled', false);
    //}

    // Persist result
    searchResult.geo_name = data.text;

    // Store 'arp_id' for conservation units
    if (data.results[0].properties.id_arp) {
      searchResult.arp_id = data.results[0].properties.id_arp;
    }
    // Or 'geocodigo' for county
    else if (data.results[0].properties.geocodigo) {
      searchResult.arp_id = data.results[0].properties.geocodigo;
    }

    searchResult.bb_ne     = data.bounds._northEast.lat + ',' + data.bounds._northEast.lng;
    searchResult.bb_sw     = data.bounds._southWest.lat + ',' + data.bounds._southWest.lng;
    searchResult.bb_ne_dsp = data.bounds._northEast.lat.toLocaleString('en',{maximumFractionDigits: 2}) + ',' + data.bounds._northEast.lng.toLocaleString('en',{maximumFractionDigits: 2});
    searchResult.bb_sw_dsp = data.bounds._southWest.lat.toLocaleString('en',{maximumFractionDigits: 2}) + ',' + data.bounds._southWest.lng.toLocaleString('en',{maximumFractionDigits: 2});

    // Fill in hidden form fields
    jQuery('#geo_name').val(searchResult.geo_name);
    jQuery('#arp_id').val(searchResult.arp_id);
    jQuery('#bb_ne').val(searchResult.bb_ne);
    jQuery('#bb_sw').val(searchResult.bb_sw);
    $searchBtn.prop('title', 'Criando alerta para ' + searchResult.geo_name + '...');

    // If search came from alert toolbox button
    if ($searchBtn.hasClass('active')) {
      // If on mobile, reopen the parent dialog and restore buttons states
      if (breakpoint() === 'default') {
        jQuery('#tab-alert').dialog('open');
        jQuery('.btn.map.alert').addClass('active');
        jQuery('.btn.map.alert').removeClass('inprogress');
        jQuery('.btn.map.layers, .btn.map.complaint, .btn.menu-btn').removeClass('inactive');
      }
      // If on desktop, also reenable tabs and return focus to the next step at parent dialog
      //else {
      //  jQuery('#tabs-toolbox').tabs('option', 'disabled', false);
      //  jQuery('#tab-alert #firstname').focus();
      //}

      // Update UI elements: area metadata, buttons states and highlight parent dialog
      jQuery('.action.area .area_name').html(searchResult.geo_name);
      jQuery('.action.area .area_coord').html(searchResult.bb_ne_dsp + '/' + searchResult.bb_sw_dsp);
      $searchBtn.addClass('selected');
      $searchBtn.removeClass('active');

      //jQuery('#tab-alert').stop(true, true).effect('highlight', {color: '#d66b58'}, 300);

      //console.debug('Alert by search was defined to ' + searchResult.geo_name + ' (' + searchResult.arp_id  + ').');
    }
    // If search was standalone
    else {
      // Clear any previous hidden form field values
      jQuery('#geo_name').val('');
      jQuery('#arp_id').val('');
      jQuery('#bb_ne').val('');
      jQuery('#bb_sw').val('');
      $searchBtn.prop('title', 'Pesquisar UCs, TIs ou municípios');

      // Fill in hidden form fields
      jQuery('#geo_name').val(searchResult.geo_name);
      jQuery('#arp_id').val(searchResult.arp_id);
      jQuery('#bb_ne').val(searchResult.bb_ne);
      jQuery('#bb_sw').val(searchResult.bb_sw);
      $searchBtn.prop('title', 'Criando alerta para ' + searchResult.geo_name + '...');

      // Update UI elements: area metadata and button state
      jQuery('.action.area .area_name').html(searchResult.geo_name);
      jQuery('.action.area .area_coord').html(searchResult.bb_ne_dsp + '/' + searchResult.bb_sw_dsp);
      $searchBtn.addClass('selected');

      //console.debug('Alert by search was defined to ' + searchResult.geo_name + ' (' + searchResult.arp_id  + ').');
    }
  });
}

// Complaint form
export function addComplaintTool(map) {
  // Init draw handler
  drawComplaint = drawHandler('marker', map);

  var $drawMarkBtn = getDrawMarkBtn();

  // In complaint dialog, marker button only
  jQuery('#tab-complaint #complaint-marker-btn .fa-stack-1x').click(function () {
    // Reset other selection tools
    resetDrawRectBtn(map);
    resetSearchBtn(map);

    // Reset button state and values if previous results were set
    if ($drawMarkBtn.hasClass('selected')) {
      // Previously selected
      map.removeLayer(drawLayer);
      $drawMarkBtn.removeClass('selected');

      // Clear any previous hidden form field values
      jQuery('#marker').val('');

      //console.debug('Existing values for complaint draw has been cleared.');
    }
    // Disable handler on second click
    else if ($drawMarkBtn.hasClass('active')) {
      drawComplaint.disable();
      $drawMarkBtn.removeClass('active');

      // If on mobile, restore toolbar button states
      if (breakpoint() === 'default') {
        jQuery('.btn.map.complaint').addClass('active');
        jQuery('.btn.map.complaint').removeClass('inprogress');
        jQuery('.btn.map.layers, .btn.map.alert .btn.menu-btn').removeClass('inactive');
      }

      //console.debug('Complaint draw was deactivated.');
    }
    else {
      // Uptade UI elements: button state
      $drawMarkBtn.addClass('active');

      // If on mobile, close the parent dialog, set toolbar button to 'in progress' and disable the other buttons
      if (breakpoint() === 'default') {
        jQuery('#tab-complaint').dialog('close');
        jQuery('.btn.map.complaint').toggleClass('active');
        jQuery('.btn.map.complaint').addClass('inprogress');
        jQuery('.btn.map.layers, .btn.map.alert, .btn.menu-btn').addClass('inactive');
      }
      //else {
      //  // Prevent tab change
      //  jQuery('#tabs-toolbox').tabs('option', 'disabled', [0]);
      //}

      // Enable complain draw handler
      drawComplaint.enable();

      //console.debug('Drawing a complaint marker...');
    }
  });
}
