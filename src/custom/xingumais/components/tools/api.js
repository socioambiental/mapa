/**
 * Functions for node POST via Drupal 8 RESTful Web Services.
 *
 * @author <a href='https://mexapi.macpress.com.br/about'>@denydias</a>
 * @version 1.0
 * @license GPL-2.0+
 * @copyright ©2016 Instituto Socioambiental.
 */

/**
 * URL scheme and authority for requests.
 *
 * @type {string}
 */
export var protoHost = 'https://ox.socioambiental.org';

/**
 * Try to request a Drupal 8 RESTful Web Services token.
 *
 * @function getCsrfToken
 *
 * @param {string}   form     - The form ID firing the request.
 * @param {function} callback - The callback function for successful requests.
 *
 * @returns {function} Run the callback function with the supplied session token.
 * @throws  {void}     Update form with a human readable error message.
 *
 * @see {@link https://www.drupal.org/node/2269515|Drupal 8 Javascript API}
 */
export function getCsrfToken(form, callback) {
  jQuery
    .get(protoHost + '/rest/session/token')
    .fail(function() {
      if (form === '#alert-form') {
        jQuery('#tab-alert').html('<div id="alert-submsg"><h5>Ah não! Algo deu errado!</h5><p><strong>Pedimos desculpas!</strong></p><p>Alguma coisa errada ocorreu no cadastro de sua assinatura e ela não foi registrada.</p><p>Por favor, tente novamente mais tarde.</p><p style="font-size:10px;text-align:center;">O pajé disse: token.</p></div>');
      } else if (form === '#complaint-form') {
        jQuery('#tab-complaint').html('<div id="complaint-submsg"><h5>Ah, não! Algo deu errado.</h5><p><strong>Pedimos desculpas!</strong></p><p>Alguma coisa errada ocorreu no cadastro da sua denúncia e ela não foi registrada.</p><p>Por favor, tente novamente mais tarde.</p><p style="font-size:10px;text-align:center;">O pajé disse: token.</p></div>');
      }
    })
    .done(function (data) {
      var csrfToken = data;
      callback(csrfToken);
    });
}

/**
 * Try to post a hal_json node object via Drupal 8 RESTful Web Services.
 *
 * @function postNode
 *
 * @param {string} csrfToken - Supplied session token.
 * @param {object} node      - A hal_json node object.
 * @param {string} form      - The form ID firing the request.
 *
 * @returns {function} Post node, inform success and send a verification email if `#alert-form`.
 * @throws  {void}     Update form with a human readable error message.
 *
 * @see {@link https://www.drupal.org/node/2269515|Drupal 8 Javascript API}
 */
export function postNode(csrfToken, node, form) {
  jQuery.ajax({
    url     : protoHost + '/node?_format=hal_json',
    method  : 'POST',
    headers : {
      'Content-Type': 'application/hal+json',
      'X-CSRF-Token': csrfToken
    },
    data: JSON.stringify(node),
    success: function (node) {
      if (form === '#alert-form') {
        // Defaults for optional node values as they may be undefined for certain use cases
        var alertLastname = '',
          alertGeoname = '';

        // Lastname field (undefined if user input was empty)
        if (typeof node.field_alert_lastname !== 'undefined') {
          alertLastname = node.field_alert_lastname[0].value;
          //if (oxOpt.d){debug('Last name set for this alert: ' + alertLastname);}
        } else {
          alertLastname = '';
          //if (oxOpt.d){debug('No last name set for this alert, default to empty.');}
        }

        // Geoname field (undefined if user drew an alert rectangle, which means a custom area with no name)
        if (typeof node.field_alert_geoname !== 'undefined') {
          alertGeoname = node.field_alert_geoname[0].value;
          //if (oxOpt.d){debug('Area name set for this alert: ' + alertGeoname);}
        }
        else {
          alertGeoname = '';
          //if (oxOpt.d){debug('No area name set for this alert, default to empty.');}
        }

        if (typeof node.field_alert_description !== 'undefined') {
          var alertDescription = node.field_alert_description[0].value;
        }
        else {
          var alertDescription = '';
        }

        // Inform success
        jQuery('#tab-alert').html('<div id="alert-submsg"><h5>Seu acompanhamento foi criado!</h5><p>Em breve você receberá uma mensagem para confirmar a criação do seu novo acompanhamento.</p><p><strong>Importante!</strong><br />Siga as instruções na mensagem para ativar e começar a receber o seu acompanhamento personalizado.</p><p>Obrigado por usar o <strong>Observatório Xingu</strong>!</p></div>');
        // Send activation email
        jQuery.ajax({
          url: protoHost + '/themes/d8ox/extras/verify.php?n=' + node.nid[0].value + '&a=' + node.field_alert_email[0].value + '&k=' + node.field_activation_key[0].value + '&f=' + node.field_alert_name[0].value + '&l=' + alertLastname + '&g=' + alertGeoname + '&d=' + alertDescription,
          method: 'GET'
        });

      }
      else if (form === '#complaint-form') {

        jQuery('#tab-complaint').html('<div id="complaint-submsg"><h5>Sua mensagem foi enviada!</h5><p>Vamos iniciar a averiguação da sua mensagem em breve.<p><p>Obrigado por usar o <strong>Observatório Xingu</strong>!</p></div>');

        jQuery.ajax({
          url    : protoHost + '/themes/d8ox/extras/notifycomplaint.php?n=' + node.nid[0].value + '&a=' + node.title[0].value,
          method : 'GET'
        });
      }

      //if (oxOpt.d){debugTable(form + ' was successfully posted as D8 node.', node);}
    },
    error: function(xhr, status, error) {
      if (form === '#alert-form') {
        jQuery('#tab-alert').html('<div id="alert-submsg"><h5>Ah, não! Algo deu errado.</h5><p><strong>Pedimos desculpas!</strong></p><p>Alguma coisa errada ocorreu no cadastro de sua assinatura e ela não foi registrada.</p><p>Por favor, tente novamente mais tarde.</p><p style="font-size:10px;text-align:center;">O pajé disse: ' + xhr.status + '.</p></div>');
      }
      else if (form === '#complaint-form') {
        jQuery('#tab-complaint').html('<div id="complaint-submsg"><h5>Ah, não! Algo deu errado.</h5><p><strong>Pedimos desculpas!</strong></p><p>Alguma coisa errada ocorreu no cadastro da sua denúncia e ela não foi registrada.</p><p>Por favor, tente novamente mais tarde.</p><p style="font-size:10px;text-align:center;">O pajé disse: ' + xhr.status + '.</p></div>');
      }

      //if (oxOpt.d){debugTable(form + ' post to D8 node has failed:', xhr);}
    }
  });
}

/**
 * Generate a random activation key.
 *
 * @function randomKey
 *
 * @param {string} length - Key lenght to generate.
 * @param {string} chars  - Character groups to use: a ⇒ lower case, A ⇒ upper case, # ⇒ numbers.
 *
 * @returns {string} A random key of the selected length.
 */
export function randomKey(length, chars) {
  var mask = '';

  if (chars.indexOf('a') > -1) mask += 'abcdefghijklmnopqrstuvwxyz';
  if (chars.indexOf('A') > -1) mask += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  if (chars.indexOf('#') > -1) mask += '0123456789';

  var result = '';

  for (var i = length; i > 0; --i) {
    result += mask[Math.round(Math.random() * (mask.length - 1))];
  }

  return result;
}
