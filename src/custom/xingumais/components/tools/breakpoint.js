/**
 * This module declares the resources required by Xingu maps.
 *
 * @version 1.0.1
 * @author <a href='https://mexapi.macpress.com.br/about'>@denydias</a>
 * @license GPL-2.0+
 * @copyright ©2016 Instituto Socioambiental
 */
export function breakpoint() {
  return window.getComputedStyle(document.querySelector('body'), ':before').getPropertyValue('content').replace(/\"/g, '');
};
