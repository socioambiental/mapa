import { drawHandler, addDrawTool, addSearchTool, addComplaintTool } from './form.js';
import { registerSubmit                                            } from './submit.js';
//import L                                                           from '../../../map/leaflet.js';
import 'tooltipster';
import 'tooltipster/dist/css/tooltipster.bundle.min.css';
import 'tooltipster/dist/css/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-punk.min.css';
import EventBus from '../../../../utils/EventBus'

export default {
  data: function() {
    return {
      // UI
      name   : null,
      state  : 'closed',

      // Controls
      searchControl: null,

      // Standard isaMap properties
      isamap : null,
      locale : null,
      map    : null,
      control: null,
      id     : null,
    }
  },
  mounted: function() {
    this.stopPropagation();
    this.listenerControls()

    // Append draw tools to the sidebar
    var $drawButtons = jQuery('#mapDraw li');

    $drawButtons.on('click', function (e) {
      e.stopPropagation();

      var $this                  = jQuery(this);
      var $drawButtonsSubActions = $this.find('.drawButtonsSubActions');

      jQuery(`.leaflet-draw-draw-${$this.attr('id')} span`).click();
      $drawButtonsSubActions.addClass('active');

      if ($drawButtonsSubActions.find('ul').length === 0) {
        jQuery('.leaflet-draw-actions').appendTo($drawButtonsSubActions);
      }
    })

    // Register drawing tool for the custom form
    addDrawTool(this.isamap.map);

    // Register search tool for the custom form
    addSearchTool(this.isamap.map, this.searchControl);

    // Register complaint tool
    addComplaintTool(this.isamap.map);

    // Register form validation
    registerSubmit(this.isamap.map);
  },
  methods: {
    listenerControls: function () {
      ['alert', 'complaint'].forEach(function (control) {
        EventBus.$on('state-box-' + control, function (value) {
          const el = jQuery('#tab-' + control)
          value ? el.addClass('active') : el.removeClass('active')
        })
      })
    },
    stopPropagation: function() {
      L.DomEvent.disableScrollPropagation(this.$el);
      L.DomEvent.disableClickPropagation(this.$el);
      L.DomEvent.on(this.$el, 'wheel', L.DomEvent.stopPropagation);
      L.DomEvent.on(this.$el, 'click', L.DomEvent.stopPropagation);
    },
    toggleControl: function() {
      jQuery('#mapToolsToggle').toggleClass('active');
      jQuery('#mapToolsMenu').toggleClass('active');

      // Remove all tooltips
      jQuery('#tab-alert .action.icons, #alert-form input[type="text"], #alert-form input[type="email"], #alert-form .fields.alerts, #alert-form input[type="checkbox"], #bb_ne, #complaint-draw, #complaint-form input[type="text"], #complaint-form textarea, #marker, .geocoder-control-input.leaflet-bar').tooltipster('close');

      // Handle search form state
      if (this.state == 'closed') {
        this.state = 'opened';

        var searchState = jQuery('#mapToolsMenu > li.search').hasClass('active');

        if (searchState) {
          // Ensure the search control is displayed
          jQuery('.geocoder-control.leaflet-control').addClass('active');
        }
      }
      else {
        this.state = 'closed';

        // Ensure the search control is hidden
        jQuery('.geocoder-control.leaflet-control').removeClass('active');
      }
    },
    toggleItem: function(event) {
      // Activate the current item
      jQuery(event.target).closest('li').toggleClass('active');

      // Activate it's target
      var target = jQuery(event.target).closest('li').attr('data-target-selector');
      jQuery(target).toggleClass('active');

      // Deactivate all other items
      jQuery(event.target).closest('li').siblings().removeClass('active');
      jQuery(event.target).closest('li').siblings().each(function () {
        jQuery(jQuery(this).attr('data-target-selector')).removeClass('active');
      })
    },
    hideControls: function() {
      if (this.state == 'opened') {
        this.toggleControl();
      }

      if (jQuery('#leaflet-layer-control-map').hasClass('leaflet-control-layers-expanded')) {
        jQuery('#leaflet-layer-control-map i.fa-window-close').click();
      }
    },
    printMap: function(event) {
      // Print map using the backend
      //var download  =  window.location.search == '' ? '?download=true' : '&download=true';
      //var href      = 'https://xingumais.org.br/download-map';
      //href         +=  window.location.pathname.replace('/custom/xingumais/develop', '').replace('/custom/xingumais', '') + window.location.search + download;
      //href         += '&print_title=' + jQuery('#mapPrintName').val();
      //
      //event.preventDefault();
      //
      ////window.location.href = href;
      //window.location.assign(href);

      // Print map using the browser
      this.hideControls();

      // Make sure everything is hidden before printing
      this.$nextTick(function () {
        document.title = jQuery('#mapPrintName').val();
        parent.window.focus();
        window.print();
      });
    },
  },
  computed: {
    sectionClass: function() {
      return 'leaflet-control ' + this.name;
    },
  },
}
