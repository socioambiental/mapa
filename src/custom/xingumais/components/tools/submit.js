/**
 * This module declares the resources required by Xingu maps.
 *
 * @version 1.0.1
 * @author <a href='https://mexapi.macpress.com.br/about'>@denydias</a>
 * @license GPL-2.0+
 * @copyright ©2016 Instituto Socioambiental
 */

import { drawLayer                                    } from './form.js';
import { protoHost, getCsrfToken, postNode, randomKey } from './api.js';
import 'jquery-validation';
import 'jquery-ui';
import 'tooltipster';

/**
 * Form validation and processing.
 *
 * @function formProcess
 *
 * @returns {void}
 *
 * @see {@link https://jqueryvalidation.org/|jQuery Validation Plugin}
 * @see {@link Tooltipster|Tooltipster}
 */
export function registerSubmit(map) {
  jQuery(document).ready(function() {
    // Add a validator method for geographic coordinates
    jQuery.validator.addMethod('latlng', function(value, element) {
      var regexLatlng = /^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?),\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)$/;

      if (regexLatlng.test(value)) {
        return true;
      } else {
        return false;
      }
    });

    // Initialize Tooltipster on form input elements
    jQuery('#tab-alert .action.icons, #alert-form input[type="text"], #alert-form input[type="email"], #alert-form .fields.alerts, #alert-form input[type="checkbox"], #bb_ne, #complaint-draw, #complaint-form input[type="text"], #complaint-form textarea, #marker, .geocoder-control-input.leaflet-bar').tooltipster({
      distance      : 2,
      timer         : 0,
      theme         : 'tooltipster-punk',
      trigger       : 'custom',
      viewportAware : true
    });

    // Validation for alert form
    jQuery('#alert-form').validate({
      rules: {
        firstname: {
          required: true, minlength: 2, maxlength: 50
        },
        lastname: {
          required: false, minlength: 2, maxlength: 100
        },
        email: {
          required: true, email: true
        },
        type: {
          required: true, minlength: 1
        },
        bb_ne: {
          required: true, latlng: true
        }
      },
      ignore: '#geo_name, #arp_id, #bb_sw, #alert_description',
      messages: {
        firstname : 'Informe seu nome (2 a 50 letras)',
        lastname  : 'Informe seu sobrenome (2 a 100 letras)',
        email     : 'Informe seu email',
        type      : 'Selecione ao menos um tipo',
        bb_ne     : 'Pesquise ou desenhe no mapa',
      },
      errorPlacement: function (error, element) {
        if (element.attr('name') === 'type') {
          jQuery('#alert-form .fields.alerts').tooltipster('content', jQuery(error).text());
          jQuery('#alert-form .fields.alerts').tooltipster('open');
        } else if (element.attr('name') === 'bb_ne') {
          jQuery('#tab-alert .action.icons').tooltipster('content', jQuery(error).text());
          jQuery('#tab-alert .action.icons').tooltipster('open');
        } else {
          jQuery(element).tooltipster('content', jQuery(error).text());
          jQuery(element).tooltipster('open');
        }
      },
      success: function (label, element) {
        if (jQuery(element).attr('name') === 'type') {
          jQuery('#alert-form .fields.alerts').tooltipster('close');
        } else if (jQuery(element).attr('name') === 'bb0') {
          jQuery('#tab-alert .action.icons').tooltipster('close');
        } else {
          jQuery(element).tooltipster('close');
        }
      },
      submitHandler: function(form) {
        // Form is valid, let's roll...
        // Compose the form data as an object
        var fireValue = jQuery('#alert-form #fire').is(':checked') ? +jQuery('#alert-form #fire:checkbox:checked').val() : 0,
          defoValue = jQuery('#alert-form #deforest').is(':checked') ? +jQuery('#alert-form #deforest:checkbox:checked').val() : 0,
          alertFormdata = [{
            name        : jQuery('#alert-form #firstname').val(),
            description : jQuery('#alert-form #alert_description').val(),
            lastname    : jQuery('#alert-form #lastname').val(),
            email       : jQuery('#alert-form #email').val(),
            type        : fireValue + defoValue,
            geoname     : jQuery('#alert-form #geo_name').val(),
            arpid       : jQuery('#alert-form #arp_id').val(),
            geofields   : [
              jQuery('#alert-form #bb_ne').val(),
              jQuery('#alert-form #bb_sw').val(),
            ],
            key: randomKey(36, '#aA')
          }];

        // The object bellow translates our form data into hal_json, D8 style
        var newNode = {
          _links: {
            type: {
              href: protoHost + '/rest/type/node/alerts'
            }
          },
          type                    : {target_id : 'alerts'},
          title                   : {value     : alertFormdata[0].name + ' ' + alertFormdata[0].lastname},
          field_alert_name        : {value     : alertFormdata[0].name},
          field_alert_description : {value     : alertFormdata[0].description},
          field_alert_lastname    : {value     : alertFormdata[0].lastname},
          field_alert_email       : {value     : alertFormdata[0].email},
          field_alert_type        : {value     : alertFormdata[0].type},
          field_alert_geoname     : {value     : alertFormdata[0].geoname},
          field_alert_arpid       : {value     : alertFormdata[0].arpid},
          field_alert_geofield    : [
            { value : alertFormdata[0].geofields[0] },
            { value : alertFormdata[0].geofields[1] }
          ],
          field_activation_key    : {value: alertFormdata[0].key},
          field_subscription      : {value: 0}
        };

        //if (oxOpt.d){debugTable('Alert form data to be sent:',alertFormdata);}

        // Call D8 tokenizer
        getCsrfToken('#alert-form', function (csrfToken) {
          // All set, post the form data
          postNode(csrfToken, newNode, '#alert-form');
        });

        // Clear map
        map.removeLayer(drawLayer);
      }
    });

    // Validation for complaint form
    jQuery('#complaint-form').validate({
      rules: {
        title: {
          required: true, minlength: 10, maxlength: 50
        },
        description: {
          required: true, minlength: 20, maxlength: 300
        },
        marker: {
          required: true, latlng: true
        }
      },
      ignore: '#file',
      messages: {
        title       : 'Elabore um título (10 a 50 letras)',
        description : 'Escreva uma descrição (20 a 300 letras)',
        marker      : 'Posicione um marcador no mapa',
      },
      errorPlacement: function (error, element) {

        if (element.attr('name') === 'marker') {
          jQuery('#complaint-draw').tooltipster('content', jQuery(error).text());
          jQuery('#complaint-draw').tooltipster('open');
        } else {
          jQuery(element).tooltipster('content', jQuery(error).text());
          jQuery(element).tooltipster('open');
        }
      },
      success: function (label, element) {
        if (jQuery(element).attr('name') === 'marker') {
          jQuery('#complaint-draw').tooltipster('close');
        } else {
          jQuery(element).tooltipster('close');
        }
      },
      submitHandler: function(form) {
        // Form is valid, let's roll...
        // Compose the form data as an object
        var complaintFormdata = [{
          title  : jQuery('#complaint-form #title').val(),
          desc   : jQuery('#complaint-form #description').val(),
          marker : jQuery('#complaint-form #marker').val(),
        }];

        // The object bellow translates our form data into hal_json, D8 style
        var newNode = {
          _links: {
            type: {
              href: protoHost + '/rest/type/node/complaints'
            }
          },
          type                   : { target_id : 'complaints'                },
          title                  : { value     : complaintFormdata[0].title  },
          field_complaint_desc   : { value     : complaintFormdata[0].desc   },
          field_complaint_marker : { value     : complaintFormdata[0].marker }
        };

        //if (oxOpt.d){debugTable('Complaint form data to be sent:',complaintFormdata);}

        // Call D8 tokenizer
        getCsrfToken('#complaint-form', function (csrfToken) {
          // All set, post the form data
          postNode(csrfToken, newNode, '#complaint-form');
        });

        // Clear map
        map.removeLayer(drawLayer);
      }
    });
  });
}
