const output            = __dirname + '/../../../data/';
const fs                = require('fs');
const async             = require('async');
const fetch             = require('node-fetch');
const config            = require('../../../config/api.js');
const processEntityItem = require('../api/entity.js');
const buildUrl          = require('../api/url.js');
const fsWriteError      = require('../fs/error.js');
const numTasks          = require('./tasks.js');
var data                = require('./data.js');
var globals             = require('../../map/globals.js');

// Aggregator for TIs
module.exports = async function () {
  var details  = {};
  var type     = 'ti';
  var tasks    = [];
  var url      = buildUrl('sisarp/v2/' + type);
  var response = await fetch(url).catch(err => console.error(err));;
  data[type]   = await response.json();

  // Ensure no API Key leaks
  delete data[type].meta.apikey;
  delete data[type].meta.endpoint;

  await async.map(Object.keys(data[type].data), function(item) {
    // Hint: Use reflect to continue the execution of other tasks when a task fails
    // See https://caolan.github.io/async/v3/docs.html#reflect
    //     https://caolan.github.io/async/v3/docs.html#parallel
    if (data[type].data[item].plotagem == 'Sim') {
      tasks.push(async.reflect(async function() {
        var url_details                   = buildUrl('sisarp/v2/' + type + '/' + data[type].data[item].id);
        var response                      = await fetch(url_details).catch(err => console.error(err));;
        details[data[type].data[item].id] = await response.json();

        await processEntityItem(item, type, 'povo',                   'povo',      'povo');
        await processEntityItem(item, type, 'bacia_'          + type, 'descricao', 'bacia');
        await processEntityItem(item, type, 'bioma_'          + type, 'descricao', 'bioma');
        await processEntityItem(item, type, 'fitofisionomia_' + type, 'descricao', 'fitofisionomia');
        await processEntityItem(item, type, 'pressao_'        + type, 'tipo',      'pressao');
        await processEntityItem(item, type, 'uf_'             + type, 'uf',        'municipio');
      }));
    }
  }, function(err, results) {});

  await async.parallelLimit(tasks, numTasks, function(err, results) {
    if (err) {
      console.log(err);
    }

    var assembly = data[type].data;

    // Build an array with id_arp as index
    // This code used to live in the frontend, at loaders.js
    for (var i = 0; i < assembly.length; i++) {
      // Exclude unplotted TIs
      if (assembly[i].plotagem == 'Não') {
        continue;
      }

      var id               = assembly[i].id;
      globals.arps.tis[id] = assembly[i];

      // Build properties
      for (var property in globals.arpsProperties.tis) {
        var currentProperty = globals.arpsProperties.tis[property];

        if (globals.arpsByProperty.tis[currentProperty] == undefined) {
          globals.arpsByProperty.tis[currentProperty] = {};
        }

        // Exeption handling
        if (currentProperty == 'jurisdicao_legal') {
          if (globals.arpsByProperty.tis[currentProperty][details[id].data.info_ti.jurisdicao_legal] == undefined) {
            globals.arpsByProperty.tis[currentProperty][details[id].data.info_ti.jurisdicao_legal] = [];
          }

          globals.arps.tis[id].jurisdicao_legal = details[id].data.info_ti.jurisdicao_legal;
          globals.arpsByProperty.tis[currentProperty][details[id].data.info_ti.jurisdicao_legal].push(id);
        }
        else if (currentProperty == 'funai') {
          if (globals.arpsByProperty.tis[currentProperty][details[id].data.info_ti.funai] == undefined) {
            globals.arpsByProperty.tis[currentProperty][details[id].data.info_ti.funai] = [];
          }

          globals.arps.tis[id].funai = details[id].data.info_ti.funai;
          globals.arpsByProperty.tis[currentProperty][details[id].data.info_ti.funai].push(id);
        }
        else if (currentProperty == 'sesai') {
          if (globals.arpsByProperty.tis[currentProperty][details[id].data.info_ti.sesai] == undefined) {
            globals.arpsByProperty.tis[currentProperty][details[id].data.info_ti.sesai] = [];
          }

          globals.arps.tis[id].sesai = details[id].data.info_ti.sesai;
          globals.arpsByProperty.tis[currentProperty][details[id].data.info_ti.sesai].push(id);
        }
        else if (currentProperty == 'situacao_juridica') {
          if (globals.arpsByProperty.tis[currentProperty][details[id].data.info_ti.situacao_juridica] == undefined) {
            globals.arpsByProperty.tis[currentProperty][details[id].data.info_ti.situacao_juridica] = [];
          }

          globals.arps.tis[id].situacao_juridica = details[id].data.info_ti.situacao_juridica;
          globals.arpsByProperty.tis[currentProperty][details[id].data.info_ti.situacao_juridica].push(id);
        }
        else if (currentProperty == 'grupo_etapa') {
          if (globals.arpsByProperty.tis[currentProperty][details[id].data.info_ti.grupo_etapa] == undefined) {
            globals.arpsByProperty.tis[currentProperty][details[id].data.info_ti.grupo_etapa] = [];
          }

          globals.arps.tis[id].grupo_etapa = details[id].data.info_ti.grupo_etapa;
          globals.arpsByProperty.tis[currentProperty][details[id].data.info_ti.grupo_etapa].push(id);
        }
        else if (currentProperty == 'uf') {
          for (var uf in globals.arps.tis[id].uf) {
            var uf = globals.arps.tis[id].municipio[uf].uf;

            if (globals.arpsByProperty.tis[currentProperty][uf] == undefined) {
              globals.arpsByProperty.tis[currentProperty][uf] = [];
            }

            globals.arpsByProperty.tis[currentProperty][uf].push(id);
          }
        }
        else if (currentProperty == 'presenca_isolados') {
          if (globals.arpsByProperty.tis[currentProperty][details[id].data.info_ti.presenca_isolados] == undefined) {
            globals.arpsByProperty.tis[currentProperty][details[id].data.info_ti.presenca_isolados] = [];
          }

          globals.arps.tis[id].presenca_isolados = details[id].data.info_ti.presenca_isolados;
          globals.arpsByProperty.tis[currentProperty][details[id].data.info_ti.presenca_isolados].push(id);
        }
        else if (currentProperty == 'populacao') {
          var n = details[id].data.info_ti.populacao_arp;

          if (n == 0 || n == null) {
            var range = 0;
          }
          else if (n <= 25) {
            var range = 25;
          }
          else if (n >= 26 && n <= 50) {
            var range = 50;
          }
          else if (n >= 51 && n <= 100) {
            var range = 100;
          }
          else if (n >= 101 && n <= 200) {
            var range = 200;
          }
          else if (n >= 201 && n <= 500) {
            var range = 500;
          }
          else if (n >= 501 && n <= 1000) {
            var range = 1000;
          }
          else if (n >= 1001 && n <= 5000) {
            var range = 5000;
          }
          else if (n >= 5001) {
            var range = 5001;
          }

          if (globals.arpsByProperty.tis[currentProperty][range] == undefined) {
            globals.arpsByProperty.tis[currentProperty][range] = [];
          }

          globals.arps.tis[id].populacao = n;
          globals.arpsByProperty.tis[currentProperty][range].push(id);
        }
        else if (currentProperty == 'area') {
          var n = globals.arps.tis[id].area_oficial;

          if (n == 0 || n == null) {
            var range = 0;
          }
          else if (n <= 10000) {
            var range = 10000;
          }
          else if (n >= 10001 && n <= 50000) {
            var range = 50000;
          }
          else if (n >=  50001 && n <= 250000) {
            var range = 250000;
          }
          else if (n >= 250001 && n <= 500000) {
            var range = 500000;
          }
          else if (n >=  500001 && n <= 1500000) {
            var range = 1500000;
          }
          else if (n >= 1500001) {
            var range = 1500001;
          }

          if (globals.arpsByProperty.tis[currentProperty][range] == undefined) {
            globals.arpsByProperty.tis[currentProperty][range] = [];
          }

          globals.arpsByProperty.tis[currentProperty][range].push(id);
        }
        else if (currentProperty == 'faixa_fronteira') {
          if (globals.arpsByProperty.tis[currentProperty][details[id].data.info_ti.faixa_fronteira] == undefined) {
            globals.arpsByProperty.tis[currentProperty][details[id].data.info_ti.faixa_fronteira] = [];
          }

          globals.arps.tis[id].faixa_fronteira = details[id].data.info_ti.faixa_fronteira;
          globals.arpsByProperty.tis[currentProperty][details[id].data.info_ti.faixa_fronteira].push(id);
        }
        else if (currentProperty == 'total_povos_residentes') {
          var total = globals.arps.tis[id].povo.length;

          if (globals.arpsByProperty.tis[currentProperty][total] == undefined) {
            globals.arpsByProperty.tis[currentProperty][total] = [];
          }

          globals.arps.tis[id].total_povos_residentes = total;
          globals.arpsByProperty.tis[currentProperty][total].push(id);
        }
        else if (globals.arps.tis[id][currentProperty] == undefined || globals.arps.tis[id][currentProperty] == null) {
          continue;
        }
        else {
          // Default case
          if (globals.arpsByProperty.tis[currentProperty][globals.arps.tis[id][currentProperty]] == undefined) {
            globals.arpsByProperty.tis[currentProperty][globals.arps.tis[id][currentProperty]] = [];
          }

          globals.arpsByProperty.tis[currentProperty][globals.arps.tis[id][currentProperty]].push(id);
        }
      }

      // Remove some data to decrease JSON output
      delete globals.arps.tis[id].povo;
      delete globals.arps.tis[id].bacia;
      delete globals.arps.tis[id].bioma;
      delete globals.arps.tis[id].fitofisionomia;
      delete globals.arps.tis[id].pressao;
      delete globals.arps.tis[id].municipio;
    }

    globals.arpsByProperty.tis.povo           = data.povo;
    globals.arpsByProperty.tis.bacia          = data['bacia_'          + type];
    globals.arpsByProperty.tis.bioma          = data['bioma_'          + type];
    globals.arpsByProperty.tis.fitofisionomia = data['fitofisionomia_' + type];
    globals.arpsByProperty.tis.pressao        = data['pressao_'        + type];

    var result = {
      //tis      : data.ti,
      tis        : globals.arps.tis,
      properties : globals.arpsByProperty.tis,
    };

    // Write output
    fs.writeFile(output   + "sisarp/v2/tis.json",                          JSON.stringify(result                                 ), fsWriteError);
    //fs.writeFile(output + "sisarp/v2/tis.json",                          JSON.stringify(result,                         null, 2), fsWriteError);
    //fs.writeFile(output + "sisarp/v2/tis.json",                          JSON.stringify(data[type],                     null, 2), fsWriteError);
    //fs.writeFile(output + "sisarp/v2/povos.json",                        JSON.stringify(data.povo,                      null, 2), fsWriteError);
    //fs.writeFile(output + "sisarp/v2/bacias_"          + type + '.json', JSON.stringify(data['bacia_'          + type], null, 2), fsWriteError);
    //fs.writeFile(output + "sisarp/v2/biomas_"          + type + '.json', JSON.stringify(data['bioma_'          + type], null, 2), fsWriteError);
    //fs.writeFile(output + "sisarp/v2/fitofisionomias_" + type + '.json', JSON.stringify(data['fitofisionomia_' + type], null, 2), fsWriteError);
    //fs.writeFile(output + "sisarp/v2/pressoes_"        + type + '.json', JSON.stringify(data['pressao_'        + type], null, 2), fsWriteError);
  });
}
