const output            = __dirname + '/../../../data/';
const fs                = require('fs');
const async             = require('async');
const fetch             = require('node-fetch');
const config            = require('../../../config/api.js');
const processEntityItem = require('../api/entity.js');
const buildUrl          = require('../api/url.js');
const fsWriteError      = require('../fs/error.js');
const numTasks          = require('./tasks.js');
var data                = require('./data.js');

// Aggregator for UFs
module.exports = async function () {
  var url      = buildUrl('sisarp/v2/uf');
  var response = await fetch(url).catch(err => console.error(err));;
  var data     = await response.json();

  // Ensure no API Key leaks
  delete data.meta.apikey;
  delete data.meta.endpoint;

  // Write output
  //fs.writeFile(output + "sisarp/v2/ufs.json", JSON.stringify(data, null, 2), fsWriteError);
  fs.writeFile(output + "sisarp/v2/ufs.json",   JSON.stringify(data         ), fsWriteError);
}
