const config = require('../../../config/api.js');

// Build a complete REST endpoint URL
module.exports = function (endpoint, params = {}, auth = 'apikey') {
  if (Object.keys(params).length !== 0 && params.constructor === Object) {
    str  = '';
    args = [];

    for (var item in params) {
      if (params[item] != null && params[item] != '') {
        args.push( item + '=' + params[item]);
      }
    }

    if (auth == 'apikey') {
      args.push('apikey=' + config.apikey);
    }

    params = (args.length > 0) ? '?' + args.join('&') : null;
  }
  else {
    if (auth == 'apikey') {
      params = '?apikey=' + config.apikey;
    }
    else {
      params = '';
    }
  }

  return config.apibase + endpoint + params;
}
