const fetch    = require('node-fetch');
const buildUrl = require('../api/url.js');
var   data     = require('../aggregate/data.js');

// Retrieve entity data for a given ARP and append it to the entity list
module.exports = async function (item, type, entity, key, service) {
  //console.log('Processing ARP ' + data[type].data[item].id + ' for type ' + type + ' and entity ' + entity + '...');

  var url      = buildUrl('sisarp/v2/' + service, { 'arp': data[type].data[item].id });
  var response = await fetch(url).catch(err => console.error(err));
  var datum    = await response.json();

  if (data[entity] == undefined) {
    data[entity] = { };
  }

  // Ensure no API Key leaks
  delete datum.meta.apikey;
  delete data[type].meta.endpoint;

  // Save a copy into the data type
  data[type].data[item][service] = datum.data;

  for (var element in datum.data) {
    if (data[entity][datum.data[element][key]] == undefined) {
      data[entity][datum.data[element][key]] = [];
    }

    data[entity][datum.data[element][key]].push(data[type].data[item].id);
  }
}
