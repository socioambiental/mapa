const config  = require('../../config/api.js');
const Headers = require('node-fetch').Headers;

/**
 * Build headers for a REST request.
 *
 * To be used like
 *
 *   fetch(url, {
 *     method : 'GET',
 *     headers: headers,
 *   })
 */
export function (auth = 'apikey') {
  var headers = new Headers();

  if (auth == 'password') {
    headers.set('Authorization', 'Basic ' + base64.encode(config.apiuser + ":" + config.apipassword));
  }

  return headers;
}
