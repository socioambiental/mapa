    <?php
    $isSecure = false;

    // Thanks https://stackoverflow.com/questions/1175096/how-to-find-out-if-youre-using-https-without-serverhttps/16076965#16076965
    if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
      $isSecure = true;
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' || !empty($_SERVER['HTTP_X_FORWARDED_SSL']) && $_SERVER['HTTP_X_FORWARDED_SSL'] == 'on') {
      $isSecure = true;
    }

    $protocol = $isSecure ? 'https://' : 'http://';
    $url      = $protocol . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
    $endpoint = $protocol . $_SERVER['SERVER_NAME'] . '/oembed?url=' . urlencode($url);
    ?>

    <!-- oEmbed -->
    <link rel="alternate" type="application/json+oembed"
                          id="oembed-json"
                          href="<?php print $endpoint; ?>&format=json"
                          title="Mapa Socioambiental" />
    <link rel="alternate" type="text/xml+oembed"
                          id="oembed-xml"
                          href="<?php print $endpoint; ?>&format=xml"
                          title="Mapa Socioambiental" />
