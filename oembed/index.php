<?php
/**
 * oEmbed endpoint.
 * See https://oembed.com
 */

// Params
$url       = $_REQUEST['url'];
$format    = $_REQUEST['format'];
$maxheight = $_REQUEST['maxheight'];
$maxwidth  = $_REQUEST['maxwidth'];

// Determine height and width
$height = ($maxheight == NULL) ? '300' : (int) $maxheight;
$width  = ($maxwidth  == NULL) ? '600' : (int) $maxwidth;

// Validate format
$format = ($format == NULL) ? 'json' : (string) $format;

// Determine url
$url = ($url != NULL) ? urldecode($url) : 'https://mapa.eco.br/v2/';

// Check the URL
// See https://oembed.com/#section3
if (!preg_match('/^https:\/\/mapa\.eco\.br/', $url)) {
  header('HTTP/1.0 404 Not Found');
  exit;
}
else if ($url == 'https://mapa.eco.br' || $url == 'https://mapa.eco.br/') {
  $url = 'https://mapa.eco.br/v2/';
}
//else if ($url == NULL) {
//  header('HTTP/1.0 404 Not Found');
//  exit;
//}
// Unreliable, see https://stackoverflow.com/questions/7003416/validating-a-url-in-php
//else if (filter_var($url, FILTER_VALIDATE_URL)) {
//  header('HTTP/1.0 404 Not Found');
//  exit;
//}

// Our response
$response = array(
  'type'          => 'rich',
  'version'       => '1.0',
  'provider_name' => 'Instituto Socioambiental',
  'provider_url'  => 'https://www.socioambiental.org',
  'height'        => $height,
  'width'         => $width,
  'html'          => '<iframe src="'. $url .'" height="'. $height .'" width="'. $width  .'" allowfullscreen="1" frameborder="0"></iframe>',
);

// Set header
if ($format == 'json') {
  header('Content-Type: application/json');
}
else if ($format == 'xml') {
  header('Content-Type: text/xml');
}
else {
  //header('HTTP/1.0 404 Not Found');
  //exit;
  // JSON by default
  $format == 'json';
  header('Content-Type: application/json');
}

// Print output
if ($format == 'json') {
  print json_encode($response);
}
else {
  echo '<?xml version="1.0" encoding="utf-8" standalone="yes"?>';
  echo '  <oembed>';
  echo '  <type>'.          $response['type']                   .'</type>';
  echo '  <version>'.       $response['version']                .'</version>';
  echo '  <provider_name>'. $response['provider_name']          .'</provider_name>';
  echo '  <provider_url>'.  $response['provider_url']           .'</provider_url>';
  echo '  <height>'.        $response['height']                 .'</height>';
  echo '  <width>'.         $response['width']                  .'</width>';
  //echo '  <html>'.        htmlspecialchars($response['html']) .'</html>';
  echo '  <html>'.          $response['html'] .'</html>';
  echo '</oembed>';
}
