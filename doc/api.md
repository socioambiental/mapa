# API

## V1

* Check `self.methods.*` at `jquery.isamap.js` for a full list of callable methods.
* Check available settings using

    jQuery('#map').isaMap('dumpSettings');
